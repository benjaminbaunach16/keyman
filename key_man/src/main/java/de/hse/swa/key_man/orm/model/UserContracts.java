package de.hse.swa.key_man.orm.model;

import javax.persistence.*;

@Entity
@Table(name = "USER_CONTRACT")
public class UserContracts {

    @Id
    @SequenceGenerator(name = "UserContSeq", sequenceName = "ZSEQ_USERCONT_ID", allocationSize = 1, initialValue = 10)
    @GeneratedValue(generator = "UserContSeq")

    @Column(name = "idUserContracts")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "USER_ID")
    private User user;

    @ManyToOne
    @JoinColumn(name = "SERVICECONTRACT_ID")
    private ServiceContracts serviceContract;

    public UserContracts(){    }

    //id
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }

    //user
    public User getUser() {
        return user;
    }
    public void setUser(User user) {
        this.user = user;
    }

    //ServiceContracts
    public ServiceContracts getServiceContract() {
        return serviceContract;
    }
    public void setServiceContract(ServiceContracts serviceContract) {
        this.serviceContract = serviceContract;
    }
}