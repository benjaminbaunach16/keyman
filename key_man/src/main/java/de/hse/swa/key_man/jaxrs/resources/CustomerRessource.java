package de.hse.swa.key_man.jaxrs.resources;


import de.hse.swa.key_man.orm.dao.CustomerDao;
import de.hse.swa.key_man.orm.dao.UserContractsDao;
import de.hse.swa.key_man.orm.model.Customer;
import de.hse.swa.key_man.orm.model.UserContracts;
import io.vertx.core.http.HttpServerRequest;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.util.List;



@RequestScoped
@Path("/customers")
public class CustomerRessource {

    @Inject
    CustomerDao customerDao;

    @Inject
    UserContractsDao userContractsDao;

    @Context
    HttpServerRequest request;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Customer> getCustomers() {
        return customerDao.getCustomers() ;
    }



    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Customer getCustomer(@PathParam("id") Integer id) {
//        System.out.println(id);
        return customerDao.getCustomer(id) ;
    }



    @PUT
    @Path("put/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Customer updatePerson(Customer customer, @PathParam("id") Integer id) {
        return customerDao.updatePerson(id, customer);

    }



    @DELETE
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public void removePerson(@PathParam("id") Integer idcustomer) {customerDao.removePerson(idcustomer);}


    @POST
    @Path("addCustomer")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Customer addCustomer(Customer customer) {
        return customerDao.addCustomer(customer);
    }




}
