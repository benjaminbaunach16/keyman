package de.hse.swa.key_man.orm.dao;



import de.hse.swa.key_man.orm.model.Customer;
import de.hse.swa.key_man.orm.model.ServiceContracts;
import de.hse.swa.key_man.orm.model.User;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import javax.ws.rs.core.Response;

@ApplicationScoped
public class ServiceContractDao {

    @Inject
    EntityManager em;

    public List<ServiceContracts> getServiceContracts() {
        TypedQuery<ServiceContracts> query = em.createQuery("SELECT u FROM ServiceContracts u", ServiceContracts.class);
        List<ServiceContracts> results = query.getResultList();
        return results;
    }

    public ServiceContracts getServiceContracts(Integer id) {
        return em.find(ServiceContracts.class, id);
    }

    public List<ServiceContracts> getContracts(User user    ) {
        TypedQuery<ServiceContracts> query = em.createQuery(
                "SELECT contract FROM ServiceContracts AS contract JOIN contract.users user WHERE user.id = :CONTRACT",
                ServiceContracts.class);
        query.setParameter("CONTRACT",user.getId());
        List<ServiceContracts> results = query.getResultList();
        return results;
    }


    @Transactional
    public ServiceContracts save(ServiceContracts sc) {
        if (sc.getIdservicecontracts() != null) {
            sc = em.merge(sc);
        } else {
            em.persist(sc);
        }
        return sc;
    }

    @Transactional
    public void removeServiceContracts(ServiceContracts serviceContracts) {
        em.remove(serviceContracts);
    }

    @Transactional
    public void addUserToContract(User user, ServiceContracts serviceContracts) {
        serviceContracts.addUser(user);
        save(serviceContracts);

    }


    @Transactional
    public ServiceContracts addContract(ServiceContracts serviceContracts) {
        em.persist(serviceContracts);
        return serviceContracts;
    }

    @Transactional
    public void removeAllContracts() {
        try {

            Query del = em.createQuery("DELETE FROM ServiceContracts WHERE idservicecontracts >= 0");
            del.executeUpdate();

        } catch (SecurityException | IllegalStateException  e) {
            e.printStackTrace();
        }

        return;
    }

    @Transactional
    public Response removeContract(Integer idservicecontracts) {
        ServiceContracts contractsToDelete = em.find(ServiceContracts.class, idservicecontracts);

        if(contractsToDelete != null){
            em.remove(contractsToDelete);
        }

        return  Response.ok().build();
    }

    @Transactional
    public List<ServiceContracts> getContractsForUser(User user) {

        TypedQuery<ServiceContracts> query = em.createQuery(
                "SELECT u FROM ServiceContracts AS u JOIN u.users user WHERE user.id = :userId",
                ServiceContracts.class);
        query.setParameter("userId", user.getId());
        List<ServiceContracts> results = query.getResultList();

        // Gib das Ergebnis zurück
        return results;
    }

    @Transactional
    public ServiceContracts updateContracsIp(ServiceContracts serviceContracts){
        em.merge(serviceContracts);
        return serviceContracts;
    }


    @Transactional
    public ServiceContracts updateContract(Integer id, ServiceContracts contract) {
        ServiceContracts contractToUpdate = em.find(ServiceContracts.class, id);
        contractToUpdate.setContract_name(contract.getContract_name());
        contractToUpdate.setStartDatum(contract.getStartDatum());
        contractToUpdate.setEndDatum(contract.getEndDatum());
        contractToUpdate.setIp1(contract.getIp1());
        contractToUpdate.setIp2(contract.getIp2());
        contractToUpdate.setIp3(contract.getIp3());
        contractToUpdate.setNumfeldFa1(contract.getNumfeldFa1());
        contractToUpdate.setNumfeldFa2(contract.getNumfeldFa2());
        contractToUpdate.setNumfeldFa3(contract.getNumfeldFa3());
        contractToUpdate.setVersion(contract.getVersion());
        contractToUpdate.setTexfeldLizenz(contract.getTexfeldLizenz());

        return contractToUpdate;
    }


}