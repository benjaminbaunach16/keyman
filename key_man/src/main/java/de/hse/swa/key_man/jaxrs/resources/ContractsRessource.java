package de.hse.swa.key_man.jaxrs.resources;


import de.hse.swa.key_man.orm.dao.CustomerDao;
import de.hse.swa.key_man.orm.dao.ServiceContractDao;
import de.hse.swa.key_man.orm.dao.UserContractsDao;
import de.hse.swa.key_man.orm.model.Customer;
import de.hse.swa.key_man.orm.model.ServiceContracts;
import de.hse.swa.key_man.orm.model.User;
import de.hse.swa.key_man.orm.model.UserContracts;
import io.vertx.core.http.HttpServerRequest;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.util.List;

@RequestScoped
@Path("/contracts")
public class ContractsRessource {

    @Inject
    ServiceContractDao serviceContractDao;
    @Inject
    EntityManager em;

    @Inject
    CustomerDao customerDao;

    @Inject
    UserContractsDao userContractsDao;

    @Context
    HttpServerRequest request;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<ServiceContracts> getServiceContracts() {
        return serviceContractDao.getServiceContracts();
    }


    @POST
    @Path("addContracts/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ServiceContracts addContract(@PathParam("id") Integer id, ServiceContracts serviceContracts) {
        Customer customerToFind = em.find(Customer.class, id);
        serviceContracts.setIdcustomer(customerToFind);
        return serviceContractDao.addContract(serviceContracts);
    }

    @PUT
    @Path("updateContracts")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ServiceContracts save(ServiceContracts serviceContracts) {
        return serviceContractDao.save(serviceContracts);
    }

    @GET
    @Path("userContracts")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UserContracts> getAllUserContracts() {
        return userContractsDao.getAllUserContracts();
    }


    @DELETE
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public void removeContract(@PathParam("id") Integer id) {serviceContractDao.removeContract(id);}

    @PUT
    @Path("updateContractIp")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ServiceContracts updateUser(ServiceContracts serviceContracts) {

        ServiceContracts contractsToFind = em.find(ServiceContracts.class, serviceContracts.getIdservicecontracts());

        contractsToFind.setIp1(serviceContracts.getIp1());
        contractsToFind.setIp2(serviceContracts.getIp2());
        contractsToFind.setIp3(serviceContracts.getIp3());


        ServiceContracts contractsToUpdate = serviceContractDao.updateContracsIp(contractsToFind);

        return contractsToUpdate;
    }


    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public ServiceContracts getContract(@PathParam("id") Integer id) {
//        System.out.println(id);
        return serviceContractDao.getServiceContracts(id) ;
    }


    @PUT
    @Path("updateContracts/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ServiceContracts updateContract(ServiceContracts contract, @PathParam("id") Integer id) {


        return serviceContractDao.updateContract(id, contract);

    }

}

