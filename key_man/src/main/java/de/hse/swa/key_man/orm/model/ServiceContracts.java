package de.hse.swa.key_man.orm.model;


import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name ="SERVICECONTRACTS")
public class ServiceContracts {
    @Id
    @SequenceGenerator(name = "ServSeq", sequenceName = "ZSEQ_SERVICECONTRACTS_ID", allocationSize = 1, initialValue = 10)
    @GeneratedValue(generator = "ServSeq")

    @Column(name = "idservicecontracts")
    private Integer idservicecontracts;

    @Basic
    @Column(name ="contract_name")
    private String contract_name;

    @Basic
    @Column(name = "start_datum")
    private Timestamp startDatum;

    @Basic
    @Column(name = "end_datum")
    private Timestamp endDatum;

    @Basic
    @Column(name = "ip_1")
    private String ip1;

    @Basic
    @Column(name = "ip_2")
    private String ip2;

    @Basic
    @Column(name = "ip_3")
    private String ip3;

    @Basic
    @Column(name = "version")
    private String version;

    @Basic
    @Column(name = "numfeld_fa1")
    private Integer numfeldFa1;

    @Basic
    @Column(name = "numfeld_fa2")
    private Integer numfeldFa2;

    @Basic
    @Column(name = "numfeld_fa3")
    private Integer numfeldFa3;

    @Basic
    @Column(name = "texfeld_lizenz")
    private String texfeldLizenz;


    @ManyToOne
    @JoinColumn(name = "idcust", referencedColumnName = "idcustomer")
    private Customer idcustomer;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name = "USER_CONTRACT",
                joinColumns =@JoinColumn(name= "SERVICECONTRACT_ID"),
                inverseJoinColumns = @JoinColumn(name="USER_ID")
    )
    @JsonbTransient
    private Set<User> users = new HashSet<>();

    //id
    public Integer getIdservicecontracts() {
        return idservicecontracts;
    }
    public void setIdservicecontracts(Integer idservicecontracts) {
        this.idservicecontracts = idservicecontracts;
    }

    //name
    public String getContract_name() {
        return contract_name;
    }
    public void setContract_name(String contract_name) {
        this.contract_name = contract_name;
    }

    //start Datum
    public Timestamp getStartDatum() {
        return startDatum;
    }
    public void setStartDatum(Timestamp startDatum) {
        this.startDatum = startDatum;
    }

    //end Datum
    public Timestamp getEndDatum() {
        return endDatum;
    }
    public void setEndDatum(Timestamp endDatum) {
        this.endDatum = endDatum;
    }

    //ip1
    public String getIp1() {
        return ip1;
    }
    public void setIp1(String ip1) {
        this.ip1 = ip1;
    }

    //ip2
    public String getIp2() {
        return ip2;
    }
    public void setIp2(String ip2) {
        this.ip2 = ip2;
    }

    //ip3
    public String getIp3() {
        return ip3;
    }
    public void setIp3(String ip3) {
        this.ip3 = ip3;
    }

    //version
    public String getVersion() {
        return version;
    }
    public void setVersion(String version) {
        this.version = version;
    }

    //numfeld1
    public Integer getNumfeldFa1() {
        return numfeldFa1;
    }
    public void setNumfeldFa1(Integer numfeldFa1) {
        this.numfeldFa1 = numfeldFa1;
    }

    //numfled2
    public Integer getNumfeldFa2() {
        return numfeldFa2;
    }
    public void setNumfeldFa2(Integer numfeldFa2) {
        this.numfeldFa2 = numfeldFa2;
    }

    //numfled3
    public Integer getNumfeldFa3() {
        return numfeldFa3;
    }
    public void setNumfeldFa3(Integer numfeldFa3) {
        this.numfeldFa3 = numfeldFa3;
    }


    //idcostumer
    public Customer getIdcustomer() {
        return idcustomer;
    }
    public void setIdcustomer(Customer idcustomer) {
        this.idcustomer = idcustomer;
    }

    //Lizenz
    public String getTexfeldLizenz() {
        return texfeldLizenz;
    }
    public void setTexfeldLizenz(String texfeldLizenz) {
        this.texfeldLizenz = texfeldLizenz;
    }

    //add User to Contract
    public void addUser(User user){
        users.add(user);
        user.getServiceContracts().add(this);
    }

    //Remove Person from Contract
    public void RemoveUser(User user){
        users.remove(user);
        user.getServiceContracts().remove(this);
    }

    //users
    public Set<User> getUsers() {return users;}
    public void setUsers(Set<User> users) {
        this.users = users;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ServiceContracts serviceContracts = (ServiceContracts) o;
        return Objects.equals(idservicecontracts, serviceContracts.idservicecontracts);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idservicecontracts);
    }


    public void removeUser(User user) {
        users.remove(user);
        user.getServiceContracts().remove(this);
        //        it = persons.iterator();
//        while(it.hasNext()){
//        	Person p = it.next();
//           System.out.println(p.getId() + ": " + p.getUsername());
//        }
    }

}
