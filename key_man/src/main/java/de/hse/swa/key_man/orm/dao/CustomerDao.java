package de.hse.swa.key_man.orm.dao;

import de.hse.swa.key_man.orm.model.Customer;
import de.hse.swa.key_man.orm.model.User;


import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import javax.ws.rs.core.Response;

/*TEST von Selina*/
@ApplicationScoped
public class CustomerDao {

    @Inject
    EntityManager em;

    public List<Customer> getCustomers() {
        TypedQuery<Customer> query = em.createQuery("SELECT d FROM Customer d", Customer.class);
        List<Customer> results = query.getResultList();
        return results;
    }


    @Transactional
    public Customer getCustomer(Integer idcustomer) {
        Customer customerToFind = em.find(Customer.class, idcustomer);
        System.out.println("Test "+ customerToFind);

        return customerToFind ;
    }



    @Transactional
    public Customer updatePerson(Integer idcustomer, Customer customer) {
        Customer customerToUpdate = em.find(Customer.class, idcustomer);
        customerToUpdate.setName(customer.getName());
        customerToUpdate.setAdresse(customer.getAdresse());
        customerToUpdate.setDepartment(customer.getDepartment());
        return customerToUpdate;
    }


    /**
     * Update an existing department
     * @param user
     * @return the new departments with the id set
     */

    @Transactional
    public Customer addCustomer(Customer customer) {
        em.persist(customer);
        return customer;
    }

    @Transactional
    public Customer updatePerson(Customer customer) {
        em.merge(customer);
        return customer;
    }


    @Transactional
    public Response removePerson(Integer idcustomer) {
        Customer customerToDelete = em.find(Customer.class, idcustomer);

        if(customerToDelete != null){
            em.remove(customerToDelete);
        }

        return  Response.ok().build();
    }

    @Transactional
    public void removeAllCostumers() {
        try {

            Query del = em.createQuery("DELETE FROM Customer WHERE idcustomer >= 0");
            del.executeUpdate();

        } catch (SecurityException | IllegalStateException  e) {
            e.printStackTrace();
        }

        return;
    }

}
