package de.hse.swa.key_man.jaxrs.services;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class Step2GreetingService {

    public String greeting(String name) {
    	System.out.println("Hello " + name + "!!!");
        return "hello " + name;
    }

}