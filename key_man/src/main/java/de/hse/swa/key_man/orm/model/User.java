package  de.hse.swa.key_man.orm.model;

import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;


@Entity
@Table(name ="USER")
public class User {

    @Id
    @SequenceGenerator(name = "UserSeq", sequenceName = "ZSEQ_USER_ID", allocationSize = 1, initialValue = 10)
    @GeneratedValue(generator = "UserSeq")

    @Column(name = "idUser")
    private Integer id;

    @Column(name = "username", length = 64, unique = true)
    private String username;

    @Column(name = "password")
    private String password;

    @Column(name = "firstname")
    private String firstname;

    @Column(name = "lastname")
    private String lastname;

    @Column(name = "email")
    private String email;

    @Column(name = "phone1", columnDefinition = "BIGINT")
    private Long phone1;
    @Column(name = "phone2", columnDefinition = "BIGINT")
    private Long phone2;

    @Column(name = "isAdmin")
    private Byte isAdmin;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name ="idCust", referencedColumnName = "idcustomer")
    private Customer idcustomer;
    
    @ManyToMany(mappedBy = "users")
    @JsonbTransient
    private Set<ServiceContracts> serviceContracts = new HashSet<>();

    public User() {
    }

    public User(String username) {
        this.username = username;
    }

    //id
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }

    //username
    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }

    //password
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }


    //firstname
    public String getFirstname() {
        return firstname;
    }
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    //lastname
    public String getLastname() {
        return lastname;
    }
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    //email
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    //phone1
    public Long getPhone1() {
        return phone1;
    }
    public void setPhone1(Long phone1) {
        this.phone1 = phone1;
    }

    //phone2
    public Long getPhone2() {
        return phone2;
    }
    public void setPhone2(Long phone2) {
        this.phone2 = phone2;
    }

    //isAdmin
    public Byte getIsAdmin() {return isAdmin;}
    public void setIsAdmin(Byte isAdmin) {
        this.isAdmin = isAdmin;
    }

    //Customer
    public Customer getIdcustomer() {
        return idcustomer;
    }
    public void setIdcustomer(Customer idcustomer) {
        this.idcustomer = idcustomer;
    }


    //ServiceContract
    public Set<ServiceContracts> getServiceContracts() {
        return serviceContracts;
    }
    public void setServiceContracts(Set<ServiceContracts> serviceContracts) {
        this.serviceContracts = serviceContracts;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(id, user.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}



