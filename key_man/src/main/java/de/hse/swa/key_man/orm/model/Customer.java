package de.hse.swa.key_man.orm.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "CUSTOMERS")

public class Customer {

    @Id
    @SequenceGenerator(name = "CustSeq", sequenceName = "ZSEQ_CUST_ID", allocationSize = 1, initialValue = 10)
    @GeneratedValue(generator = "CustSeq")
    @Column(name = "idcustomer")
    private Integer idcustomer;
    @Basic
    @Column(name = "name")
    private String name;
    @Basic
    @Column(name = "department")
    private String department;
    @Basic
    @Column(name = "adresse")
    private String adresse;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idadmin", referencedColumnName = "iduser")
    private User iduser;


    //Constructor
    public Customer(){};
    public Customer(String name) {this.name = name;}


    //id
    public Integer getIdcustomers() {
        return idcustomer;
    }
    public void setIdcustomers(Integer idcustomers) {
        this.idcustomer = idcustomers;
    }

    //name
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    //depart
    public String getDepartment() {
        return department;
    }
    public void setDepartment(String department) {
        this.department = department;
    }

    //adresse
    public String getAdresse() {
        return adresse;
    }
    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    //idUser
    public User getIduser() {return iduser;}
    public void setIduser(User iduser) {this.iduser = iduser;}

    public String getCusname() {
        return name;
    }
}