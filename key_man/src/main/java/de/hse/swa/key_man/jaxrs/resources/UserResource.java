package de.hse.swa.key_man.jaxrs.resources;


import de.hse.swa.key_man.orm.dao.ServiceContractDao;
import de.hse.swa.key_man.orm.dao.CustomerDao;
import de.hse.swa.key_man.orm.dao.UserContractsDao;
import de.hse.swa.key_man.orm.dao.UserDao;
import de.hse.swa.key_man.orm.model.Customer;
import de.hse.swa.key_man.orm.model.ServiceContracts;
import de.hse.swa.key_man.orm.model.User;
import de.hse.swa.key_man.orm.model.UserContracts;
import io.vertx.core.http.HttpServerRequest;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.util.List;


@RequestScoped
@Path("/users")
public class UserResource {

    @Inject
    UserDao userDao;

    @Inject
    ServiceContractDao contractDao;

    @Inject
    EntityManager em;

    @Inject
    CustomerDao customerDao;

    @Inject
    UserContractsDao userContractsDao;

    @Context
    HttpServerRequest request;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<User> getUsers() {
        return userDao.getUsers();
   }

    @GET
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public User getUserWithId(@PathParam("id") Integer id) {
        return userDao.getUser(id);
    }

    @GET
    @Path("updateUser/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public User getUser(@PathParam("id") Integer id) {
        return userDao.getUser(id);
    }

    @POST
    @Path("addUser1/{idCust}/{idContract}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public User addUser(@PathParam("idCust") Integer id,@PathParam("idContract") Integer idContract, User user) {


        System.out.println(id);
        System.out.println(idContract);

        Customer customer = customerDao.getCustomer(id); // Abrufen des Kunden anhand der ID
        user.setIdcustomer(customer); // Zuweisen des Kunden an den Benutzer

        userDao.addUser(user);

        if(idContract != 0){
            User userToFind = em.find(User.class, user.getId());
            ServiceContracts serviceContracts = em.find(ServiceContracts.class, idContract);
            UserContracts userContracts = new UserContracts();
            userContracts.setUser(userToFind);
            userContracts.setServiceContract(serviceContracts);

            System.out.println(userContracts.getUser().getId());
            System.out.println(userContracts.getServiceContract().getIdservicecontracts());
            userContractsDao.save(userContracts);
        }

        return user;

    }

    @PUT
    @Path("updateUserByAdmin/{username}/{idcustomer}/{idcontract}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public User updateUserByAdmin(@PathParam("username") String username ,@PathParam("idcustomer") Integer idcustomer, @PathParam("idcontract") Integer idContract, User user) {

        User userToFind = em.find(User.class, user.getId());

        if(idcustomer != 0){
            Customer customerTofind = em.find(Customer.class, idcustomer);
            userToFind.setIdcustomer(customerTofind);
        }
        if(user.getIsAdmin() != null){
            userToFind.setIsAdmin(user.getIsAdmin());
        }
        if(user.getEmail() != null){
            userToFind.setEmail(user.getEmail());
        }
        if(user.getFirstname() != null){
            userToFind.setFirstname(user.getFirstname());
        }
        if(user.getLastname() != null){
            userToFind.setLastname(user.getLastname());
        }
        if(user.getPhone1() != null){
            userToFind.setPhone1(user.getPhone1());
        }
        if(user.getPhone2() != null){
            userToFind.setPhone2(user.getPhone2());
        }
        if(user.getUsername() != null){
            userToFind.setUsername(user.getUsername());
        }
        if(user.getPassword() != null){
            userToFind.setPassword(user.getPassword());
        }
        if (!"not".equals(username)) {
            userToFind.setUsername(username);
        }
        if(idContract != 0){
            ServiceContracts serviceContracts = em.find(ServiceContracts.class, idContract);
            UserContracts userContracts = new UserContracts();
            userContracts.setUser(userToFind);
            userContracts.setServiceContract(serviceContracts);
            System.out.println(userContracts.getUser().getId());
            System.out.println(userContracts.getServiceContract().getIdservicecontracts());
            userContractsDao.addUserContract(userContracts);
        }

        return userDao.updateUser(userToFind);
    }

    @PUT
    @Path("updateUser")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public User updateUser(User user) {
        User userToFind = em.find(User.class, user.getId());
        userToFind.setFirstname(user.getFirstname());
        userToFind.setLastname(user.getLastname());
        userToFind.setEmail(user.getEmail());
        userToFind.setPhone1(user.getPhone1());
        userToFind.setPhone2(user.getPhone2());
        userToFind.setPassword(user.getPassword());
        userToFind.setUsername(user.getUsername());
        User userToUpdate = userDao.updateUser(userToFind);

        return userToUpdate;
    }

    @DELETE
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public void removeUserByID(@PathParam("id") Integer id) {userDao.removeUser(id);}

    @DELETE
    @Path("deleteAll")
    @Produces(MediaType.APPLICATION_JSON)
    public void removeAllUsers() {
        userDao.removeAllUsers();
    }


    @POST
    @Path("login")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public User login(User user) {
        System.out.println(user.getUsername());
        return userDao.login(user.getUsername(), user.getPassword());
    }

    @GET
    @Path("/contracts/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<ServiceContracts> getContractsForUser(@PathParam("id") Integer id) {
        User user = userDao.getUser(id);
        return contractDao.getContractsForUser(user);
    }


    @POST
    @Path("addUser/{iduser}/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public User addUser1(@PathParam("id") Integer id, @PathParam("iduser") Integer iduser, Customer customer, User user)  {
        user = userDao.getUser(iduser);
        customer = customerDao.getCustomer(id);
        user.setIdcustomer(customer);
        userDao.save(user);
        return user;
 //       return userDao.save(user);

    }

    @PUT
    @Path("updateUser/{iduser}/CustomerID/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public User updateUser1(@PathParam("id") Integer id, @PathParam("iduser") Integer iduser, Customer customer, User user)  {
        user = userDao.getUser(iduser);
        customer = customerDao.getCustomer(id);
        user.setIdcustomer(customer);
        userDao.save(user);
        return user;
        //       return userDao.save(user);

    }

    @DELETE
    @Transactional
    @Path("deleteContract/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public void removeContract(@PathParam("id") Integer id){
        em.remove(em.getReference(UserContracts.class, id));
    }

}

