package de.hse.swa.key_man.orm.dao;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import javax.transaction.Transactional;
import javax.ws.rs.core.Response;


import de.hse.swa.key_man.orm.model.Customer;
import de.hse.swa.key_man.orm.model.User;
import org.jboss.logging.Logger;

import java.util.List;


@ApplicationScoped
public class UserDao {
    @Inject
    EntityManager em;

    @Inject
    CustomerDao customerDao;

    private static final Logger LOGGER = Logger.getLogger(UserDao.class);


    public List<User> getUsers() {
        TypedQuery<User> query = em.createQuery("SELECT u FROM User u", User.class);
        List<User> results = query.getResultList();
        return results;
    }


    public User getUser(Integer id) {
        return em.find(User.class, id);
    }

    @Transactional
    public User save(User user) {
        if (user.getId() != null) {
            user = em.merge(user);
        } else {
            em.persist(user);
        }
        return user;
    }

    @Transactional
    public Customer updatePerson(Customer customer) {
        em.merge(customer);
        return customer;
    }

    @Transactional
    public User addUser(User user) {
        em.persist(user);
        return user;
    }

    @Transactional
    public User updateUser(User user) {
        em.merge(user);
        return user;
    }

    @Transactional
    public Response removeUser(Integer Iduser) {
        User userToDelete = em.find(User.class, Iduser);
        if(userToDelete != null){
            em.remove(userToDelete);
        }

        return  Response.ok().build();
    }

    @Transactional
    public void removeUserByID(Integer idUser){
        try {
            System.out.println(idUser);
            em.getTransaction().begin();
            Query query = em.createQuery("Delete  from User  where id = :id");
            query.setParameter("id", idUser);
            query.executeUpdate();
            em.getTransaction().commit();
            em.close();

        } catch (SecurityException | IllegalStateException  e) {
            e.printStackTrace();
        }
        return;
    }

    @Transactional
    public void removeAllUsers() {
        try {

            Query del = em.createQuery("DELETE FROM User WHERE id >= 0");
            del.executeUpdate();

        } catch (SecurityException | IllegalStateException  e) {
            e.printStackTrace();
        }

        return;
    }

    public User login(String username, String password) {
        try {
            LOGGER.debug("Checking for user name, password Admin");
            return (User) em.createQuery("SELECT u FROM User u WHERE u.username=:username AND " + "u.password=:password")
                    .setParameter("username", username)
                    .setParameter("password", password).getSingleResult();

        } catch(NoResultException e) {
            // User u = new User();
            // u.setUser_id(0L);
            // return u;
            return null;
        }
    }
}
