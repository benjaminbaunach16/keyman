package de.hse.swa.key_man.orm.dao;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import de.hse.swa.key_man.orm.model.UserContracts;

import java.util.List;

@ApplicationScoped
public class UserContractsDao {

    @Inject
    EntityManager em;

    public UserContracts getUserContract(Integer id) {
        return em.find(UserContracts.class, id);
    }


    public List<UserContracts> getAllUserContracts() {
        TypedQuery<UserContracts> query = em.createQuery("SELECT uc FROM UserContracts uc", UserContracts.class);
        List<UserContracts> results = query.getResultList();
        return results;
    }

    @Transactional
    public UserContracts addUserContract(UserContracts userContract) {
        em.merge(userContract);
        return userContract;
    }

    @Transactional
    public UserContracts save(UserContracts userContract) {
        if (userContract.getId() != null) {
            userContract = em.merge(userContract);
        } else {
            em.persist(userContract);
        }
        return userContract;
    }

}