package de.hse.swa.key_man.jaxquarkus;

import de.hse.swa.key_man.orm.model.ServiceContracts;
import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.equalTo;


@QuarkusTest
public class ContracstRessourceTest {


    @Test
    public void testContractEndpoint() {
        given()
                .when().get("contracts")
                .then()
                .statusCode(200);
    }

    @Test
    public void testContractEndpointUserContracts() {
        given()
                .when().get("contracts/userContracts")
                .then()
                .statusCode(200);
    }

    @Test
    public void testContractByIDEndpoint() {
        given()
                .when().get("contracts/30")
                .then()
                .statusCode(200);
    }


    @Test
    public void testCreateContract() {
        ServiceContracts sc = new ServiceContracts();
        sc.setContract_name("contractTest1");
       // sc.setStartDatum(2022-12-12 10:58:13);
        //yyyy-mm-dd hh:mm:ss.[fff...]
       // sc.setEndDatum(null);
        sc.setIp1("1");
        sc.setIp2("1");
        sc.setIp3("1");
        sc.setNumfeldFa1(1);
        sc.setNumfeldFa2(1);
        sc.setNumfeldFa3(1);
        sc.setVersion("1");
        sc.setTexfeldLizenz("1");


        given()
                .contentType(ContentType.JSON)
                .body(sc)
                .pathParam("id", 14)
                .when()
                .post("contracts/addContracts/{id}")
                .then()
                .statusCode(200)
                .body("contract_name", is("contractTest1"),
                       // "starteDate", is(null),
                     //   "endDate", is(null),

                        "ip1", is("1"),
                        "ip2", is("1"),
                        "ip3", is("1"),
                        "numfeldFa1", is(1),
                        "numfeldFa2", is(1),
                        "numfeldFa1", is(1),
                        "version", is("1"),
                        "texfeldLizenz", is("1")

                );
    }

/*

    @Test
    public void testUpdateContract() {
        ServiceContracts sc = new ServiceContracts();
        sc.setContract_name("contractTest1");
        // sc.setStartDatum(2022-12-12 10:58:13);
        //yyyy-mm-dd hh:mm:ss.[fff...]
        // sc.setEndDatum(null);
        sc.setIp1("1");
        sc.setIp2("1");
        sc.setIp3("1");
        sc.setNumfeldFa1(1);
        sc.setNumfeldFa2(1);
        sc.setNumfeldFa3(1);
        sc.setVersion("1");
        sc.setTexfeldLizenz("1");


        given()
                .contentType(ContentType.JSON)
                .body(sc)
                .pathParam("id", 14)
                .when()
                .put("contracts/updateContracts/{id}")
                .then()
                .statusCode(200)
                .body("contract_name", is("contractTest1"),
                        // "starteDate", is(null),
                        //   "endDate", is(null),

                        "ip1", is("1"),
                        "ip2", is("1"),
                        "ip3", is("1"),
                        "numfeldFa1", is(1),
                        "numfeldFa2", is(1),
                        "numfeldFa1", is(1),
                        "version", is("1"),
                        "texfeldLizenz", is("1")

                );
    }
*/


}
