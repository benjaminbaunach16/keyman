package de.hse.swa.key_man.jaxquarkus;

import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.Test;
import de.hse.swa.key_man.orm.model.User;
import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.equalTo;



@QuarkusTest
public class UserRessourceTest {


    @Test
    public void testUserEndpoint() {
        given()
                .when().get("users")
                .then()
                .statusCode(200);
    }

    @Test
    public void testUserByIDEndpoint() {
        given()
                .when().get("users/47")
                .then()
                .statusCode(200);
    }




    @Test
    public void testGetByID() {
        User user = new User();
        user.setId(47);
        user.setEmail("Stefan@hs-esslingen.de");
        user.setUsername("dadfghgg");
        user.setPassword("fdgksdggsg");
        user.setFirstname("Stefan Tafferner");
        user.setLastname("Tafferner");
        user.setPhone1(0L);
        user.setPhone1(0L);


        given()
                .pathParam("id", 47)
                .when().get("/users/{id}")
                .then()
                .statusCode(200)
                .body("id", is(47),
                        "username", is("dadfghgg"),
                        "password", is("fdgksdggsg"),
                        "firstname", is("Stefan Tafferner"),
                        "lastname", is("Tafferner"),
                        "phone1", is(0),
                        "phone2", is(0)
                );
    }



    //Achtung hier wird ein User in DB eingefügt, immer neuen Namen etc machen

    @Test
    public void testCreateUser() {
        User user = new User();
        user.setFirstname("Hans");
        user.setLastname("Peter");
        user.setUsername("PeterSeinHansi");

        given()
                .contentType(ContentType.JSON)
                .body(user)
                .pathParam("idCust", 47)
                .when()
                .post("/users/addUser/{idCust}")
                .then()
                .statusCode(200)
                .body("firstname", is("Hans"),
                        "lastname", is("Peter"),
                        "username", is("PeterSeinHans")
                );
    }




    @Test
    public void testLogin() {
        User user = new User();
        user.setUsername("a");
        user.setPassword("a");

        given()
                .contentType("application/json")
                .body(user)
                .when()
                .post("users/login")
                .then()
                .statusCode(200)
                .body("username", equalTo("a"))
                .body("password", equalTo("a"));
    }

    //Dieser Test schlägt fehl, da dieser User nicht existiert
    @Test
    public void testFailureLogin() {
        User user = new User();
        user.setUsername("testUser");
        user.setPassword("TesUser");

        given()
                .contentType("application/json")
                .body(user)
                .when()
                .post("users/login")
                .then()
                .statusCode(200)
                .body("username", equalTo("testUser"))
                .body("password", equalTo("TesUser"));
    }



}
