package de.hse.swa.key_man.orm;


import static org.junit.jupiter.api.Assertions.assertEquals;

import javax.inject.Inject;

import de.hse.swa.key_man.orm.dao.CustomerDao;
import de.hse.swa.key_man.orm.dao.UserDao;


import de.hse.swa.key_man.orm.model.Customer;
import de.hse.swa.key_man.orm.model.ServiceContracts;
import de.hse.swa.key_man.orm.model.User;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Set;


@QuarkusTest
public class UserCustomerDaoTest {

    @Inject
    UserDao userDao;

    @Inject
    CustomerDao customerDao;

    private Customer createCustomer(String postfix) {
        Customer customer = new Customer();
        customer.setDepartment("Department "+ postfix);
        return customer;
    }

    private User createUser(String postfix) {
        User user = new User();
        user.setUsername("User " + postfix);
        user.setPassword("xyz");
        return user;
    }

    public void addTwoUsers() {
        User first = createUser("First");
        userDao.addUser(first);
        User second = createUser("Second");
        userDao.addUser(second);
    }

    private void printUser(User user) {
        System.out.println("id: " + user.getId() + " - " + user.getUsername());
        if (user.getIdcustomer() != null) {
            System.out.println("Customer: " + user.getIdcustomer().getDepartment());
        }
        Set<ServiceContracts> serviceContracts = user.getServiceContracts();
        for (ServiceContracts  serviceContract: serviceContracts ) {
            System.out.println("  Contract: " + serviceContract.getIdservicecontracts() + ": " + serviceContract.getContract_name());
        }
    }

    @BeforeEach
    public void clearAllFromDatabase() {
        userDao.removeAllUsers();
        customerDao.removeAllCostumers();
    }

    @Test
    void addUsersWithDepartment() {
        User first = createUser("First");
        User second = createUser("Second");

        Customer adep = createCustomer("Adep");
        customerDao.addCustomer(adep);

        first.setIdcustomer(adep);
        second.setIdcustomer(adep);

        userDao.addUser(first);
        userDao.addUser(second);

        List<User> users = userDao.getUsers();
        assertEquals(users.size(),2);
        printUser(users.get(1));

//		// Need to get this fresh
//		adep = users.get(1).getDepartment();
//
//		users = adep.getPersons();
//		assertEquals(users.size(),2);
//		printUser(users.get(0));
    }


}
