package de.hse.swa.key_man.orm;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import javax.inject.Inject;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import de.hse.swa.key_man.orm.dao.ServiceContractDao;
import de.hse.swa.key_man.orm.model.ServiceContracts;


import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
public class ServiceContractsDaoTest {

    @Inject
    ServiceContractDao serviceContractDao;

    private ServiceContracts createContracts(String postfix) {
        ServiceContracts serviceContracts = new ServiceContracts();
        serviceContracts.setContract_name("Project "+postfix);
        return serviceContracts;
    }

    public void addTwoContracs() {
        ServiceContracts first = createContracts("One");
        serviceContractDao.save(first);
        ServiceContracts second = createContracts("Two");
        serviceContractDao.save(second);
    }

    private void printContracs(ServiceContracts serviceContracts) {
        System.out.println("id: " + serviceContracts.getIdservicecontracts());
        System.out.println("Contractname: " + serviceContracts.getContract_name());
//		List<Project> projects = project.getProjects();
//		for (Project project: projects) {
//			System.out.println("  Project " + project.getId() + ": " + project.getProjectname());
//		}
    }

    @BeforeEach
    public void clearAllFromDatabase() {
        serviceContractDao.removeAllContracts();
    }

    @Test
    void addContract_1() {
        ServiceContracts first = createContracts("One");
        serviceContractDao.save(first);
        List<ServiceContracts> serviceContracts = serviceContractDao.getServiceContracts();
        assertEquals(serviceContracts.size(),1);
        printContracs(serviceContracts.get(0));
    }

    @Test
    void addContract_2() {
        addTwoContracs();
        List<ServiceContracts> serviceContracts = serviceContractDao.getServiceContracts();
        assertEquals(serviceContracts.size(),2);
        printContracs(serviceContracts.get(1));
    }
}
