package de.hse.swa.key_man.orm;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import de.hse.swa.key_man.orm.dao.CustomerDao;
import de.hse.swa.key_man.orm.dao.UserDao;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import de.hse.swa.key_man.orm.model.Customer;
import de.hse.swa.key_man.orm.model.ServiceContracts;
import de.hse.swa.key_man.orm.model.User;
import de.hse.swa.key_man.orm.model.UserContracts;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;
import de.hse.swa.key_man.orm.dao.ServiceContractDao;

@QuarkusTest
public class UserServiceContractsDaoTest {
    @Inject
    EntityManager em;

    @Inject
    UserDao ud;

    @Inject
    CustomerDao cd;

    @Inject
    ServiceContractDao svd;


    private ServiceContracts createContracts(String postfix) {
        ServiceContracts serviceContracts = new ServiceContracts();
        serviceContracts.setContract_name("Contracts "+postfix);
        return serviceContracts;
    }
    public void addTwoContracs() {
        ServiceContracts first = createContracts("One");
        svd.save(first);
        ServiceContracts second = createContracts("Two");
        svd.save(second);
    }

    private Customer createCustomer(String prefix) {
        Customer customer = new Customer();
        customer.setName(prefix+"Customer");
        return customer;
    }

    private User createUser(String prefix) {
        User user = new User();
        user.setUsername(prefix+"UserName");
        user.setPassword(prefix+"xyz");
        user.setIsAdmin((byte) 0);
        return user;
    }


    private void printUser(User user) {
        System.out.println("id: " + user.getId());
        System.out.println("Username: " + user.getUsername());
    }
     @BeforeEach
     public void clearAllFromDatabase() {

        svd.removeAllContracts();
        ud.removeAllUsers();
        cd.removeAllCostumers();
    }


    @Test
    @Transactional
    void testCreateReadUserContracts() {
        User user = createUser("Test");
        user = ud.addUser(user); // Füge einen neuen User in die Datenbank ein

        ServiceContracts serviceContracts = createContracts("Test");
        serviceContracts = svd.save(serviceContracts); // Füge einen neuen ServiceContracts in die Datenbank ein

        UserContracts userContracts = new UserContracts();
        userContracts.setUser(user);
        userContracts.setServiceContract(serviceContracts);

        em.persist(userContracts); // Speichere den neuen UserContracts-Eintrag in der Datenbank

        UserContracts savedUserContracts = em.find(UserContracts.class, userContracts.getId()); // Lese den UserContracts-Eintrag aus der Datenbank

        assertEquals(userContracts.getUser().getId(), savedUserContracts.getUser().getId()); // Vergleiche die User-IDs
        assertEquals(userContracts.getServiceContract().getIdservicecontracts(), savedUserContracts.getServiceContract().getIdservicecontracts()); // Vergleiche die

    }

}

