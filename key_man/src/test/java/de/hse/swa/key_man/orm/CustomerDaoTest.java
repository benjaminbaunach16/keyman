package de.hse.swa.key_man.orm;

import de.hse.swa.key_man.orm.dao.CustomerDao;
import de.hse.swa.key_man.orm.model.Customer;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@QuarkusTest
public class CustomerDaoTest {
/*BLA*/
    @Inject
    CustomerDao customerDao;

    private Customer createCustomer(String prefix) {
        Customer customer = new Customer();
        customer.setName(prefix+"DepartmentName");
        return customer;
    }

    public void addTwoCustomer() {
        Customer first = createCustomer("firstDep");
        customerDao.addCustomer(first);
        Customer second = createCustomer("secondDep");
        customerDao.addCustomer(second);
    }

    private void printDepartment(Customer customer) {
        System.out.println("id: " + customer.getIdcustomers());
        System.out.println("Customer: " + customer.getName());
//		List<Project> projects = department.getProjects();
//		for (Project project: projects) {
//			System.out.println("  Project " + project.getId() + ": " + project.getProjectname());
//		}
    }


    //tests

    @BeforeEach
    public void clearAllFromDatabase() {
        customerDao.removeAllCostumers();
    }


    @Test
    void addCustomer_1() {
        Customer first = createCustomer("first");
        customerDao.addCustomer(first);
        List<Customer> custs = customerDao.getCustomers();
        assertEquals(custs.size(),1);
        printDepartment(custs.get(0));
    }

    @Test
    void addCustomer_2() {
        addTwoCustomer();
        List<Customer> custs = customerDao.getCustomers();
        assertEquals(custs.size(),2);
        printDepartment(custs.get(1));
    }
}
