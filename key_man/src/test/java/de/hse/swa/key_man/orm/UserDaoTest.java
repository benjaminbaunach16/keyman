package de.hse.swa.key_man.orm;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.List;

import javax.inject.Inject;



import de.hse.swa.key_man.orm.model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import de.hse.swa.key_man.orm.dao.UserDao;
import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
public class UserDaoTest {

    @Inject
    UserDao userDao;

    private User createUser(String prefix, Byte isAdmin) {
        User user = new User();
        user.setUsername(prefix+"UserName");
        user.setPassword(prefix+"xyz");
        user.setIsAdmin(isAdmin);
        return user;
    }
//
    public void addTwoUsers() {
        User first = createUser("First", (byte) 0);
        userDao.addUser(first);
        User second = createUser("Second" , (byte) 0);
        userDao.addUser(second);
    }
    private void printUser(User user) {
        System.out.println("id: " + user.getId());
        System.out.println("Username: " + user.getUsername());
        System.out.println("IsAdmin: " + user.getIsAdmin());
//		List<Project> projects = person.getProjects();
//		for (Project project: projects) {
//			System.out.println("  Project " + project.getId() + ": " + project.getProjectname());
//		}
    }

    @BeforeEach
    public void clearAllFromDatabase() {
        userDao.removeAllUsers();
    }

    @Test
    void addUser_1() {
        User first = createUser("First", (byte)0);
        userDao.addUser(first);
        List<User> users = userDao.getUsers();
        assertEquals(users.size(),1);
        printUser(users.get(0));
    }

    @Test
    void addUser_2() {
        addTwoUsers();
        List<User> users = userDao.getUsers();
        assertEquals(users.size(),2);
        printUser(users.get(1));
    }

    @Test
    void checkLogin_1() {
        User first = createUser("first", (byte) 0);
        userDao.addUser(first);
        List<User> users = userDao.getUsers();
        assertNotNull(userDao.login(users.get(0).getUsername(), users.get(0).getPassword()));
        System.out.println("login success");
    }

    @Test
    void addAdmin_1(){
        User first = createUser("Admin", (byte)1);
        userDao.addUser(first);
        List<User> users = userDao.getUsers();
        assertEquals(users.size(),1);
        printUser(users.get(0));
    }

    @Test
    void checkLogin_Admin() {
        User first = createUser("Admin", (byte) 1);
        userDao.addUser(first);
        List<User> users = userDao.getUsers();
        assertNotNull(userDao.login(users.get(0).getUsername(), users.get(0).getPassword()));
        System.out.println("login success");
    }

}
