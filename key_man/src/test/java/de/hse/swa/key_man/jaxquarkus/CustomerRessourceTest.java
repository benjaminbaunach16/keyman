package de.hse.swa.key_man.jaxquarkus;

import de.hse.swa.key_man.orm.model.Customer;
import io.restassured.http.ContentType;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.equalTo;
import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;


@QuarkusTest
public class CustomerRessourceTest {

    @Test
    public void testCustomerEndpoint() {
        given()
                .when().get("customers")
                .then()
                .statusCode(200);
    }

    @Test
    public void testCustomerByIDEndpoint() {
        given()
                .when().get("customers/47")
                .then()
                .statusCode(200);
    }


    @Test
    public void testGetByID() {
        Customer customer = new Customer();
        customer.setIdcustomers(47);
        customer.setName("Borg Warner");
        customer.setDepartment("Rechnungswese");
        customer.setAdresse("Parkstraße 1");

        given()
                .pathParam("id", 47)
                .when().get("/customers/{id}")
                .then()
                .statusCode(200)
                .body("idcustomers", is(47),
                        "name", is("Borg Warner"),
                        "department", is("Rechnungswese"),
                        "adresse", is("Parkstraße 1")

                );
    }


    @Test
    public void testAddCustomer() {
        Customer customer = new Customer();
        customer.setAdresse("Straße Unit");
        customer.setName("John Doe");
        customer.setDepartment("C-Klasse AMG");

        given()
                .contentType(ContentType.JSON)
                .body(customer)
                .when()
                .post("/customers/addCustomer")
                .then()
                .statusCode(200)
                .body("adresse", is("Straße Unit"),
                        "name", is("John Doe"),
                        "department", is("C-Klasse AMG")
                );
    }

}
