import React, {useEffect, useState} from "react";
import "./css/App.css"
import Login from './Login';
import Admin from "./Admin";
import User from "./User"
import axios from "axios";
import * as url from "./Url";


//const theUrl ="http://localhost:8080/";

const theUrl =url.path;



function App() {
	const [loggedIn, setLoggedIn] = useState(false);
	const [isAdmin, setIsAdmin] = useState('');
	const [data, setData] = useState({});
	const [, setUser] = useState({});

	const authorized = dataToGet => {
		setLoggedIn(true);
		setIsAdmin(dataToGet.isAdmin);
		setData(dataToGet);
	};

	useEffect(() => {
		if (data && data.id) {
			console.log("Test from URL")
			axios
				.get(theUrl + `/users/${data.id}`)
				.then(response => {
					setUser(response.data);
				})
				.catch(error => {
					console.error(error);
				});
		}
	}, [data]);


	if (loggedIn && isAdmin) {
		return <Admin url={theUrl + "/"} />;
	}
	if (loggedIn && data) {
		return <User url={theUrl + "/"} user={data} />;
	} else {
		return <Login url={theUrl} authorized={authorized} />;
	}
}

export default App;

