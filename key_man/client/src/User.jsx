import React, {useEffect, useState} from "react";
import {BrowserRouter as Router, Route, Routes} from "react-router-dom";
import "./User/Css/Home.css"
import MenubarUser from "./User/MenubarUser";
import Home from "./User/Home"
import UserProfile from "./User/UserProfile";
import axios from "axios";
import ContractDetails from "./User/ContractDetails";
import * as url from "./Url"

const theUrl =url.path;


function User(props) {
    const [user, setUser] = useState(null);
    const [, setContracts] = useState()

    useEffect(() => {
        console.log('id: ' + props.user.id);
        // Check if the user prop exists and if it has an id property
        if (props.user && props.user.id) {
            // Make a request to the server to get the user data
            axios
                .get(theUrl + `/users/${props.user.id}`)
                .then(response => {
                    setUser(response.data);
                })
                .catch(error => {
                    console.error(error);
                });
        }
    }, [props.user]);

    //get Contracts
    useEffect(() => {


        // Make a request to the server to get the user data
        axios
            .get(theUrl + `/users/contracts/${props.user.id}`)
            .then(response => {
                setContracts(response.data);
            })
            .catch(error => {
                console.error(error);
            });

    }, [props.user]);



    // Render a loading message until the user data is available
    return (
        <div>
            <Router>
                <MenubarUser />
                <Routes>
                    <Route path="/" element={<Home />} />
                    <Route path="/Profile" element={<UserProfile user={user}/>} />
                    <Route path="/Profile/Contracts" element={<ContractDetails user={user}/>} />
                </Routes>
            </Router>
        </div>
    );
}

export default User