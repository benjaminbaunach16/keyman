import React from "react";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import "./Admin/Css/Home.css"
import MenubarAdmin from "./Admin/MenubarAdmin";
import Contracts from "./Admin/Contracts/Contracts";
import Customers from "./Admin/Customer/Customers";
import Users from "./Admin/Users/Users";
import Home from "./Admin/Home";
import {AddEdit} from "./Admin/Users/AddEdit";
import {View} from "./Admin/View";
import {AddEditCustomerid} from "./Admin/Users/AddEditCustomerid"
import AddCustomer from "./Admin/Customer/AddCustomer";
import CustomerView from "./Admin/Customer/CustomerView";
import ContractsView from "./Admin/Contracts/ContractsView";
import ContractsUpdate from "./Admin/Contracts/ContractsUpdate";

class Admin extends React.Component{

    render(){

        return (
            <div>
                <Router>
                    <MenubarAdmin></MenubarAdmin>

                    <Routes>
                        <Route path="/" element={<Home/>} />
                        <Route path="/Contracts/manage" element={<Contracts/>} />
                        <Route path="/Contracts/manage/ContractsView/:id" element={<ContractsView/>} />
                        <Route path="/Contracts/manage/updateContract/:id" element={<ContractsUpdate/>} />


                        <Route path="/Customers/manage" element={<Customers/>} />

                        <Route path="/Users/manage" element={<Users/>} />
                        <Route path="/Users/manage/addUser" element={<AddEdit/>} />
                        <Route path="/Users/manage/addUser/:customerid" element={<AddEditCustomerid/>} />
                        <Route path="/Users/manage/updateUser/:customerid/CustomerID" element={<AddEditCustomerid/>} />

                        <Route path="/Users/manage/updateUser/:id" element={<AddEdit/>} />
                        <Route path="/Users/manage/view/:id" element={<View/>} />
                        <Route path="/Customers/manage/addCustomer" element={<AddCustomer/>} />
                        <Route path="/Customers/manage/update/:id" element={<AddCustomer/>} />
                        <Route path="/Customers/manage/CustomerView/:id" element={<CustomerView/>} />
                    </Routes>


                </Router>


            </div>
        );
    }
}

export default Admin