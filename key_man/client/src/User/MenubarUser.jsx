import React from "react"
import { Link, NavLink } from "react-router-dom";

import "./Css/MenubarUser.css"

class MenubarUser extends React.Component{

    constructor(props) {
        super(props);
        this.state = {
            setButton : false,
        }
    }

    logout() {
        localStorage.clear();
        window.location.href = '/';
    }

    render(){

        return(
            <nav className="navbar">
                <div className="navbar-container">
                    <div className="menu-icon">
                        <i className={ "fas fa-times"} />
                    </div>
                    <ul className={ "nav-menu active" }>

                        <li className="nav-item">
                            <NavLink to="/"
                                  className="nav-links"
                                  activeClassName="active"
                            >
                                Home
                            </NavLink>
                        </li>


                        <li className="nav-item">
                            <NavLink
                                to="/Profile"
                                className="nav-links"
                                activeClassName="active"
                            >
                                Profil
                            </NavLink>
                        </li>

                        <li className="nav-item">
                            <Link to="/"
                                  className="nav-links"
                                  onClick={this.logout}

                            >
                                Logout
                            </Link>
                        </li>



                    </ul>
                </div>

            </nav>
        )
    }
}

export default MenubarUser