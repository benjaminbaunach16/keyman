import React, {useEffect, useState} from "react";
import axios from "axios";
import "./Css/ContractDetails.css"

import * as url from "../Url";

const theUrl =url.path;

function ContractDetails(props) {

    const [user,] = useState(props.user);
    const [contracts, setContracts] = useState()
    const [showModal1, setShowModal1] = useState(false);
    const [selectedContract, setSelectedContract] = useState(null);


    //get Contracts
    useEffect(() => {


        // Make a request to the server to get the user data
        axios
            .get(theUrl + `/users/contracts/${user.id}`)
            .then(response => {
                setContracts(response.data);
            })
            .catch(error => {
                console.error(error);
            });

    }, [user.id]);

    function openModal(contracts) {
        setShowModal1(true);
        setSelectedContract(contracts);
    }

    function closeModal() {
        setShowModal1(false);
        setSelectedContract(null);
    }

    const handleSubmit = event => {
        event.preventDefault();

        const updatedContract = {
            idservicecontracts: selectedContract.idservicecontracts,
            idcustomer: selectedContract.idcustomer,
            endDatum : selectedContract.endDatum,
            startDatum: selectedContract.startDatum,
            numfeld_fa1: selectedContract.numfeld_fa1,
            numfeld_fa2: selectedContract.numfeld_fa2,
            numfeld_fa3: selectedContract.numfeld_fa3,
            texfeld_lizenz: selectedContract.texfeld_lizenz,
            version: selectedContract.version



        };

        if (event.target.ip1.value) {
            updatedContract.ip1 = event.target.ip1.value;
        }
        if (event.target.ip2.value) {
            updatedContract.ip2 = event.target.ip2.value;
        }
        if (event.target.ip3.value) {
            updatedContract.ip3 = event.target.ip3.value;
        }


        axios
            .put(theUrl + "/contracts/updateContractIp", updatedContract)
            .then(response => {
                setShowModal1(false);
                setSelectedContract(null);
            })
            .catch(error => {
                console.error(error);
            });

        // Finde das Objekt, das du updaten möchtest
        let updatedContracts = [...contracts];
        let contractToUpdateIndex = updatedContracts.findIndex(
            contract => contract.idservicecontracts === updatedContract.idservicecontracts
        );

        // Ersetze das Objekt in der Liste
        updatedContracts[contractToUpdateIndex] = updatedContract;

        // Speichere die geänderte Liste
        setContracts(updatedContracts);
    };




    return (
        <div className="details-container">
            <h1 className="details-h1"> DETAILS </h1>

            {contracts && (
                <table className="styled-table tableU">
                <thead>
                <tr>
                    <th>Contract name</th>
                    <th>Contract start date</th>
                    <th>Contract end date</th>
                    <th>Ip 1</th>
                    <th>Ip 2</th>
                    <th>Ip 3</th>
                    <th>Feature A</th>
                    <th>Feature B</th>
                    <th>Feature C</th>
                    <th>Version</th>
                    <th>Licence</th>
                    <th> </th>
                </tr>
                </thead>
                <tbody>
                {contracts.map((contracts, index) => (
                <tr>

                    <td>{contracts.contract_name}</td>
                    <td>{contracts.startDatum}</td>
                    <td>{contracts.endDatum}</td>
                    <td>{contracts.ip1}</td>
                    <td>{contracts.ip2}</td>
                    <td>{contracts.ip3}</td>
                    <td>{contracts.version}</td>
                    <td>{contracts.numfeldFa1}</td>
                    <td>{contracts.numfeldFa2}</td>
                    <td>{contracts.numfeldFa3}</td>
                    <td>{contracts.textfeldLizenz}</td>
                    <td>
                        {contracts.endDatum < new Date().toISOString().slice(0,10) && (
                                <button className="btn btn-edit" onClick={() => openModal(contracts)}>Edit</button>
                        )}
                    </td>

                </tr>
                    ))}
                </tbody>

            </table>
                )}
            {showModal1 && (
                <div className="detailmodal-content">
                    <form  onSubmit={handleSubmit}>
                        <label>IP-1</label>
                        <input
                            type="text"
                            id ="ip1"
                            name="ip1"
                            placeholder={selectedContract.ip1}
                        />

                        <label>IP-2</label>
                        <input
                            type="text"
                            id ="ip2"
                            name="ip2"
                            placeholder={selectedContract.ip2}
                        />

                        <label>IP-3</label>
                        <input
                            type="text"
                            id ="ip3"
                            name="ip3"
                            placeholder={selectedContract.ip3}
                        />
                        <button className="button-save"> save </button>
                    </form>
                    <button className="button-delete" onClick={closeModal}>Close</button>
                </div>

            )}

        </div>
    )
}


export default ContractDetails