import React, {useEffect, useState} from "react";
import "./Css/UserProfil.css"
import axios from "axios";
import {Link} from "react-router-dom";
import * as url from "../Url";

const theUrl =url.path;

function UserProfile(props){

    const [user, setUser] = useState(props.user);
    const [contracts, setContracts] = useState("")

    //Update User
    const handleSubmit = event => {
        event.preventDefault();

        const updatedUser = {
            id: user.id,
            customer: user.idcustomer,
        };

        // Speichere den aktuellen Wert des Benutzers in einem Zwischenspeicher
        const currentFirstname = user.firstname;
        const currentLastname = user.lastname;
        const currentEmail = user.email;
        const currentPassword = user.password;
        const currentUsername = user.username;
        const CurrentPhone1 = user.phone1;
        const CurrentPhone2 = user.phone2;




        if (event.target.firstname.value) {
            updatedUser.firstname = event.target.firstname.value;
        } else {
            updatedUser.firstname = currentFirstname;
        }
        if (event.target.lastname.value) {
            updatedUser.lastname = event.target.lastname.value;
        } else {
            updatedUser.lastname = currentLastname;
        }
        if (event.target.email.value) {
            updatedUser.email = event.target.email.value;
        } else {
            updatedUser.email = currentEmail;
        }
        if (event.target.password.value) {
            updatedUser.password = event.target.password.value;
        } else {
            updatedUser.password = currentPassword;
        }
        if (event.target.username.value) {
            updatedUser.username = event.target.username.value;
        } else {
            updatedUser.username = currentUsername;
        }
        if(event.target.Phone1.value){
            updatedUser.phone1= event.target.Phone1.value;
        }
        else{
            updatedUser.phone1 = CurrentPhone1;
        }
        if(event.target.Phone2.value){
            updatedUser.phone2= event.target.Phone2.value;
        }
        else{
            updatedUser.phone2 = CurrentPhone2;
        }

        axios
            .put(theUrl + "/users/updateUser", updatedUser)
            .then(response => {
                setUser(response.data);
            })
            .catch(error => {
                console.error(error);
            });
    };

    //get Conracts

    useEffect(() => {


            // Make a request to the server to get the user data
            axios
                .get(theUrl + `/users/contracts/${user.id}`)
                .then(response => {
                    setContracts(response.data);
                })
                .catch(error => {
                    console.error(error);
                });

    }, [user.id]);


    return(
            <>
                <div className="header-container">
                <div className="customer-name">

                    <div> CUSTOMER </div>
                    <div className="contracts">{user.idcustomer.name} </div>
                </div>

                    <div className="contracts-name">

                    <div>CONTRACTS</div>
                    {contracts && (
                        contracts.map((contracts) => {
                        return <div className="contracts"> {contracts.contract_name} </div>
                        })
                     )}

                    </div>

                </div>

                <div className="profile-container">

                <div className="input-container-profile">
                    <div className="update-container">
                    <form onSubmit={handleSubmit}>
                        <div className="input-pair">
                        <label> Firstname</label>
                        <input
                            className="profile-input"
                            type="text"
                            id ="firstname"
                            name="firstname"
                            placeholder="Firstname..."

                        />

                        <label> Lastname</label>
                        <input
                            className="profile-input"
                            type="text"
                            id ="lastname"
                            name="lastname"
                            placeholder="Lastname..."
                        />

                        <label> Email</label>
                        <input
                            className="profile-input"
                            type="email"
                            id ="email"
                            name="email"
                            placeholder="Email..."
                        />

                        <label> Password </label>
                        <input
                            className="profile-input"
                            type="text"
                            id ="password"
                            name="password"
                            placeholder="Password..."
                        />

                        <label> Username</label>
                        <input
                            className="profile-input"
                            type="text"
                            id ="username"
                            name="username"
                            placeholder="Username..."
                        />
                        <label> Phone1</label>
                        <input
                            className="profile-input"
                            type="text"
                            id ="Phone1"
                            name="Phone1"
                            placeholder="Phone1..."
                        />
                        <label> Phone2</label>
                        <input
                            className="profile-input"
                            type="text"
                            id ="Phone2"
                            name="Phone2"
                            placeholder="Phone2..."
                        />
                        </div>
                        <div className="button-container">
                            <button className="button-profile"> save </button>

                        <Link to="/Profile/Contracts">
                                <button className="button-profile">Show contract details</button>
                        </Link>
                        </div>

                    </form>
                    </div>
                    <div className="unserInfo">
                        <table className="styled-table tableU">
                            <thead>
                            <tr>
                                <th>Firstname</th>
                                <th>Lastname</th>
                                <th>E-Mail</th>
                                <th>Password</th>
                                <th>Username</th>
                                <th>Phone1</th>
                                <th>Phone2</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>

                                <td>{user.firstname}</td>
                                <td>{user.lastname}</td>
                                <td>{user.email}</td>
                                <td>{user.password}</td>
                                <td>{user.username}</td>
                                <td>{user.phone1}</td>
                                <td>{user.phone2}</td>
                            </tr>
                            </tbody>

                        </table>
                    </div>
                </div>
            </div>

                
            </>
    )
}

export default UserProfile;