import React from "react";

import "./Css/Home.css"

class Home extends React.Component{

    render(){
        return(
            <div className="home-container">
                <h1 className="h1-home">
                    Welcome Home
                </h1>
            </div>
        )
    }
}

export default Home;