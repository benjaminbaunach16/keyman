import React from "react"
import { Link, NavLink} from "react-router-dom";
import "./Css/MenubarAdmin.css"

class MenubarAdmin extends React.Component{

    constructor(props) {
        super(props);
        this.state = {

        }
    }

    logout() {
        localStorage.clear();
        window.location.href = '/';
    }


    render(){

        return(
            <nav className="navbar">
                <div className="navbar-container">
                    <div className="menu-icon">
                        <i className={ "fas fa-times"} />
                    </div>
                    <ul className={ "nav-menu" }>
                        <li className="nav-item">
                            <NavLink to="/Admin"
                                     className='nav-links'
                                     activeClassName="active">
                                Home
                            </NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink
                                to="/Customers/manage"
                                className="nav-links"
                                activeClassName="active"
                            >
                                Customers
                            </NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink
                                to="/Contracts/manage"
                                className="nav-links"
                                activeClassName="active"
                            >
                                Contracts
                            </NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink
                                to="/Users/manage"
                                className="nav-links"
                                activeClassName="active"
                            >
                                Users
                            </NavLink>
                        </li>

                        <li className="nav-item">
                            <Link to="/"
                                  className="nav-links"
                                  onClick={this.logout}

                            >
                                Logout
                            </Link>
                        </li>
                    </ul>
                </div>

            </nav>
        )
    }
}

export default MenubarAdmin;