import React, {useState, useEffect} from "react";
import {useParams, Link} from "react-router-dom";
import axios from "axios";
import "./Css/View.css"
import * as url from "../Url";
const theUrl =url.path;


export const View = ()=> {
    const [User, setUser] = useState({});
    const [contracts, setContracts] = useState({})
    const [user_contract, setUser_Contracts] = useState({});


    useEffect(() => {
        axios
            .get(`http://localhost:8080/contracts/userContracts`)
            .then((resp) => setUser_Contracts({ ...resp.data }));
    }, []);

    const {id} = useParams();
    if (id !== undefined) {
        useEffect(() => {
            axios.get(theUrl + `/users/updateUser/${id}`)
                .then((resp) => setUser({...resp.data}))
        }, [])

    }

        useEffect(() => {
            axios
                .get(`http://localhost:8080/contracts/userContracts`)
                .then((resp) => setContracts({...resp.data}));
        }, [])


    let check = contracts[0]
    let userContracts = [];

    if(check) {
        let contractArray = [];
        for (let key in contracts) {
            contractArray.push({key: contracts[key]});
        }
        for(let i = 0; i < contractArray.length; i++){
            let test = contractArray[i];
            if(test.key.user.id == User.id){
                userContracts.push(contractArray[i].key.serviceContract);
            }
        }
    }

    function deleteContract(item) {
        for (let i = 0; i < Object.keys(user_contract).length; i++) {
             if(item == user_contract[i].serviceContract.idservicecontracts && User.id == user_contract[i].user.id){
                 console.log(user_contract[i].id)
                 if (window.confirm("Are you sure that you wanted to delete that User ?")) {
                     axios.delete(theUrl + `users/deleteContract/${user_contract[i].id}`)
                         .catch(function (error) {
                             console.log(error);
                         });
                 }
             }
        }
    }

    return(
        <div style={{marginTop: "150px"}}>
            <div className="card">
                <div className="card-header">
                    <p>User Contact Detail</p>
                </div>
                <div className="container">
                    <strong>ID: </strong>
                    <span>{id}</span>
                    <br />
                    <br />
                    <strong>Is Admin: </strong>
                    <span>{User.isAdmin}</span>
                    <br />
                    <br />
                    <strong>E-Mail: </strong>
                    <span>{User.email}</span>
                    <br />
                    <br />
                    <strong>Fist Name: </strong>
                    <span>{User.firstname}</span>
                    <br />
                    <br />
                    <strong>Last Name: </strong>
                    <span>{User.lastname}</span>
                    <br />
                    <br />
                    <strong>Phone 1: </strong>
                    <span>{User.phone1}</span>
                    <br />
                    <br />
                    <strong>Phone 2: </strong>
                    <span>{User.phone2}</span>
                    <br />
                    <br />
                    <strong>Username : </strong>
                    <span>{User.username}</span>
                    <br />
                    <br />
                    <strong>Customer: </strong>
                    <span> {typeof User.idcustomer === 'object'
                        ? JSON.stringify(User.idcustomer.name)
                        : User.name}</span>
                    <br />
                    <br />
                    {userContracts.map((item) => {
                        return(
                            <div>
                                <strong>Related Contract: </strong>
                                <span>{item.contract_name}</span>
                                <Link to="/Users/manage">
                                <div className="btn btn-delete" onClick={() => deleteContract(item.idservicecontracts)}> delete contract from user</div>
                                </Link>
                                <br />
                                <br />
                            </div>

                        )
                    })}

                    <Link to="/Users/manage">
                        <div className="btn btn-edit">Go Back</div>
                    </Link>

                </div>
            </div>
        </div>
    )
}