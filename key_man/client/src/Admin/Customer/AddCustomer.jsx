import React , {useState, useEffect} from 'react'
import {useParams, useNavigate} from "react-router-dom"
import axios from "axios";
import {Link} from "react-router-dom";
import * as url from "../../Url";

const theUrl =url.path;

const initialState = {
    adresse: "",
    department: "",
    name:  ""

}


 const AddCustomer = () => {
    const [state, setState] = useState(initialState);
    const {adresse, department, name} = state;
    const navigate = useNavigate();
    const {id} = useParams();


    if(id){
        useEffect( ( ) => {
            axios
                .get(theUrl + `/customers/${id}`)
                .then((resp) => setState({...resp.data}));
        }, [id])}

    const handleSubmit=(e) =>{
        e.preventDefault();
        if(!adresse){
            alert("Bitte etwas eingeben!!!")
            //  toast.error(" Plase provide value into each input Field")
        } else{
            if(!id) {
                axios
                    .post(theUrl + "/customers/addCustomer",{
                        adresse,
                        department,
                        name
                })
                    .then(() => {
                        setState({ adresse: "", department: "", name: ""});
                })
                    .catch((err) => console.log(err));

            } else {
                    axios.put(theUrl + `/customers/put/${id}`, {
                        adresse,
                        department,
                        name
                })
                    .then(()=>{
                        setState({ adresse: "", department: "", name: ""});
                })
                    .catch((err) => console.log(err));
                //   toast.success("Contact Added Successfully")
            }
            setTimeout(() => navigate("/Customers/manage/"), 500);
        }
    };
    const handleInputChange = (e) => {
        const {name, value} = e.target;
        setState({...state, [name]: value})
    }
    return (
        <div style={{marginTop: "100px"}}>
            <form style={{
                margin:"auto",
                padding: "15px",
                maxWidth: "400px",
                alignContent: "center"
            }}
                  onSubmit={handleSubmit}
            >
                <label htmlFor="adresse">Adresse</label>
                <input
                    type="text"
                    id="adresse"
                    name ="adresse"
                    placeholder='Please select your adress'
                    value={adresse || ''}
                    onChange={handleInputChange}
                />
                <label htmlFor="department">Department</label>
                {console.log("Dep" + department)}
                <input
                    type="text"
                    id="department"
                    name ="department"
                    placeholder='Your Department'
                    value={department || ""}
                    onChange={handleInputChange}
                />
                <label htmlFor="name">Name</label>
                <input
                    type="text"
                    id="name"
                    name ="name"
                    placeholder='Your Name...'
                    value={name || ""}
                    onChange={handleInputChange}
                />
                <input type="submit" value={id ? "Update" : "Save"}/>
                <Link to="/Customers/manage">
                    <input type="button" value="Go Back"/>
                </Link>
            </form>
        </div>
    );
};

export default AddCustomer;
