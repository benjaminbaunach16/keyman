import React, {useState, useEffect} from 'react';
import {useParams, Link} from "react-router-dom";
import axios from "axios";
import"../Css/View.css"
import * as url from "../../Url";

const theUrl =url.path;

const CustomerView = () => {
    const [Contracts, setContracts] = useState({})
    const [User, setUser] = useState({});
    const [Customer, setCustomer] = useState({});
    const {id} = useParams();

    useEffect( ( ) => {
        axios
            .get(theUrl+`/customers/${id}`)
            .then((resp)=>setCustomer({...resp.data}));
    }, [id])

    useEffect( ( ) => {
        axios.get(theUrl+`/users`).then((resp) => setUser({...resp.data}));
    }, [id])

    useEffect( ( ) => {
        axios.get(theUrl+"/contracts").then((resp) => setContracts({...resp.data}));
    }, [id])

    const newContracts = [];
    for (const key in Contracts) {
        let check = Contracts[key].idcustomer;
        if(check) {
            if(check.idcustomers == id){
                newContracts.push(Contracts[key]);
            }
        }
    }


    const newUsers = [];
    for (const key in User) {
        let check = User[key].idcustomer;
        if(check) {
            if(check.idcustomers == id) {
                newUsers.push(User[key]);
            }

        }
    }

    return(
        <div style={{marginTop: "150px"}}>
            <div className="card">
                <div className="card-header">
                    <p> Customer Detail</p>
                </div>
                <div className="container">
                    <strong>Customer: </strong>
                    <span>{Customer.name}</span>
                    <br />
                    <br />
                    <strong>ID Customer: </strong>
                    <span>{id}</span>
                    <br />
                    <br />
                    <strong>Adresse: </strong>
                    <span>{Customer.adresse}</span>
                    <br />
                    <br />
                    <strong>Department: </strong>
                    <span>{Customer.department}</span>
                    <br />
                    <br />
                    {newUsers.map((item) => {
                        return(
                            <div>
                                <strong>Related User: </strong>
                                <span>{item.firstname}</span>
                                <br />
                                <br />
                            </div>

                            )
                    })}
                    {newContracts.map((item) => {
                        return(
                            <div>
                                <strong>Related Contract: </strong>
                                <span>{item.contract_name}</span>
                                <br />
                                <br />
                            </div>
                        )
                    })}
                    <Link to="/Customers/manage">
                        <div className="btn btn-edit">Go Back</div>
                    </Link>

                </div>
            </div>
        </div>
    )

}

export default CustomerView