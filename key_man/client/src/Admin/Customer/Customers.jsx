import React, { useEffect, useState } from "react";
import "../Css/Customer.css";
import axios from "axios";
import { Link } from "react-router-dom";
import * as url from "../../Url";
import { useRadioGroup } from "@material-ui/core";

const theUrl = url.path;

const Customers = () => {
    const [loading, setLoading] = useState(false);
    const [searchTitle, setSearchTitle] = useState("");
    const [user, setUser] = useState("");
    const [data, setData] = useState([]);
    const [showModal, setShowModal] = useState(false);




    useEffect(() => {
        axios
            .get(theUrl + "/users")
            .then((response) => {
                setUser(response.data);
            })

            .catch((error) => {
                console.error(error);
            });
    }, [user]);



    const loadData = async () => {
        setLoading(true);
        const response = await axios.get(theUrl + "/customers");
        setData(response.data);
        setLoading(false);
    };


    useEffect(() => {
        loadData();
    }, []);


    const deleteContact = (id) => {
        let checker = true;

        for (let i = 0; i < user.length; i++) {
            if(user[i].idcustomer){
                if (id == user[i].idcustomer.idcustomers) {
                console.log(id);
                console.log(user[i].idcustomer.idcustomers);
                setShowModal(true);
                checker = true;
                break;
            }
            } else{

                checker = false
            }

        }


        if(!checker) {
            if (window.confirm("Are you sure that you wanted to delete that User ?")) {
                axios.delete(theUrl + `/customers/${id}`);
                //  toast.success("User deleted successfully");
                setTimeout(() => loadData(), 500);
            }
        }
    };
    function closeModal() {
        setShowModal(false);
    }

    return (
        <div style={{ marginTop: "150px" }}>
            <Link to={"/Customers/manage/addCustomer"}>
                <button className="btn btn-addUser">Add Customer</button>
            </Link>
            <div className={"SearchDiv"}>
                {/*<h3>Search Filter</h3>*/}
                <input
                    style={{ width: "30%", height: "25px" }}
                    type={"text"}
                    placeholder={"Search..."}
                    onChange={(e) => setSearchTitle(e.target.value)}
                />
            </div>
            <table className="styled-table">
                <thead>
                <tr>
                    <th style={{ textAlign: "center" }}>Number</th>
                    <th style={{ textAlign: "center" }}>ID Customer</th>
                    <th style={{ textAlign: "center" }}>Adresse</th>
                    <th style={{ textAlign: "center" }}>Department</th>
                    <th style={{ textAlign: "center" }}>Name</th>
                    <th style={{ textAlign: "center" }}>Action</th>
                </tr>
                </thead>
                <tbody>
                {loading ? (
                    <h4>Loading...</h4>
                ) : (
                    data
                        .filter((value) => {
                            if (searchTitle === "") {
                                return value;
                            } else if (
                                value.name.toLowerCase().includes(searchTitle.toLowerCase())
                            ) {
                                return value;
                            }
                        })
                        .map((item, index) => {
                            return (
                                <tr key={item.idcustomers} className={"table-tr"}>
                                    <th className={"table-th"} scope={"row"}>
                                        {index + 1}
                                    </th>
                                    <td>{item.idcustomers}</td>
                                    <td>{item.adresse}</td>
                                    <td>{item.department}</td>
                                    <td>{item.name}</td>
                                    <td>
                                        <Link to={`/Customers/manage/update/${item.idcustomers}`}>
                                            <button className="btn btn-edit">Edit</button>
                                        </Link>
                                        <button
                                            className="btn btn-delete"
                                            onClick={() => deleteContact(item.idcustomers)}
                                        >
                                            Delete
                                        </button>
                                        <Link
                                            to={`/Customers/manage/CustomerView/${item.idcustomers}`}
                                        >
                                            <button className="btn btn-view">View</button>
                                        </Link>
                                    </td>
                                </tr>
                            );
                        })
                )}
                </tbody>
            </table>
            {showModal && (
                <div className="customer-modal">
                    <div className="modal-center">
                    <h1>
                        Can't delete, because users are related! <br />
                        Delete related users first!
                    </h1>


                    <button className="button-close" onClick={closeModal}>
                        Close
                    </button>
                    </div>
                </div>
            )}
        </div>
    );
};

export default Customers;
