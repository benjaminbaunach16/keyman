import React, { useEffect, useState } from "react";

import "./Contracts.css";
import axios from "axios";
import { Link, useNavigate, useParams } from "react-router-dom";
import * as url from "../../Url";

const theUrl = url.path;

const Contracts = () => {
    const [data, setData] = useState([]);
    const [state, setState] = useState("");
    const [showModalAdd, setShowModalAdd] = useState(false);
    const [, setUserContracts] = useState([]);

    //  useEffect(() => {
    //      axios.get(theUrl + "/contracts/userContracts")
    //          .then((resp) => setUserContracts(resp.data));
    //  }, []);
    const loadData = async () => {
        const response = await axios.get(theUrl + "/contracts");
        setData(response.data);
    };
    useEffect(() => {
        loadData();
    }, []);

    //DELETE
    const deleteContract = (contractId) => {
        if (
            window.confirm("Are you sure that you wanted to delete that Contranct ?")
        ) {
            axios.delete(`http://localhost:8080/contracts/${contractId}`);
            //  toast.success("User deleted successfully");
            setTimeout(() => loadData(), 500);
        }
    };
    //ADD
    const handleAdd = (event) => {
        event.preventDefault();

        let idcustomer = event.target.idcustomer.value;

        const contractToAdd = {
            contract_name: event.target.contractName.value,
            startDatum: event.target.startDate.value,
            endDatum: event.target.endDate.value,
            ip1: event.target.ip1.value,
            ip2: event.target.ip2.value,
            ip3: event.target.ip3.value,
            numfeldFa1: event.target.featureA.value,
            numfeldFa2: event.target.featureB.value,
            numfeldFa3: event.target.featureC.value,
            version: event.target.version.value,
            texfeldLizenz: event.target.license.value,
        };

        axios
            .post(theUrl + `/contracts/addContracts/${idcustomer}`, contractToAdd)
            .then((response) => {
                setState({
                    contract_name: "",
                    startDatum: "",
                    endDatum: "",
                    ip1: "",
                    ip2: "",
                    ip3: "",
                    numfeldFa1: "",
                    numfeldFa2: "",
                    numfeldFa3: "",
                    texfeldLizenz: "",
                    version: "",
                });
            })
            .catch((error) => {
                console.error(error);
            });
        closeModalAdd();
        setTimeout(() => loadData(), 500);
    };

    function openModalAdd() {
        setShowModalAdd(true);
    }
    function closeModalAdd() {
        setShowModalAdd(false);
    }
    const handleInputChange = (e) => {
        const { name, value } = e.target;
        setState({ ...state, [name]: value });
    };
    return (
        <div style={{ marginTop: "150px" }} className="contracts-container1">
            <button onClick={openModalAdd} className="contracts-button">
                Add Contract
            </button>
            <div className="table-container">
                <table className="styled-table tableU U">
                    <thead>
                    <tr>
                        <th>Contract name</th>
                        <th>Contract start date</th>
                        <th>Contract end date</th>
                        <th>Ip 1</th>
                        <th>Ip 2</th>
                        <th>Ip 3</th>
                        <th>Feature A</th>
                        <th>Feature B</th>
                        <th>Feature C</th>
                        <th>Version</th>
                        <th>Licence</th>
                        <th>Customer</th>
                        <th> </th>
                    </tr>
                    </thead>
                    <tbody>
                    {data.map((contracts, index) => (
                        <tr>
                            <td>{contracts.contract_name}</td>
                            <td>{contracts.startDatum}</td>
                            <td>{contracts.endDatum}</td>
                            <td>{contracts.ip1}</td>
                            <td>{contracts.ip2}</td>
                            <td>{contracts.ip3}</td>
                            <td>{contracts.numfeldFa1}</td>
                            <td>{contracts.numfeldFa2}</td>
                            <td>{contracts.numfeldFa3}</td>
                            <td>{contracts.texfeldLizenz}</td>
                            <td>{contracts.version}</td>

                            <td>
                                {" "}
                                {typeof contracts.idcustomer === "object"
                                    ? JSON.stringify(contracts.idcustomer.cusname)
                                    : contracts.cusname}
                            </td>
                            <td>
                                <Link
                                    to={`/Contracts/manage/updateContract/${contracts.idservicecontracts}`}
                                >
                                    <button className="btn btn-edit">Edit</button>
                                </Link>
                                <button
                                    className="btn btn-delete"
                                    onClick={() => deleteContract(contracts.idservicecontracts)}
                                >
                                    Delete
                                </button>
                                <Link
                                    to={`/Contracts/manage/ContractsView/${contracts.idservicecontracts}`}
                                >
                                    <button className="btn btn-view">View</button>
                                </Link>
                            </td>
                        </tr>
                    ))}
                    </tbody>
                </table>
            </div>
            {showModalAdd && (
                <div className="modal">
                    <div className="modal-content">
                        <form onSubmit={handleAdd}>
                            <div className="form-group">
                                <label htmlFor="contractName" className="form-label">
                                    Contract Name:
                                    <input
                                        type="text"
                                        id="contractName"
                                        name="contractName"
                                        className="form-input"
                                    />
                                </label>
                                <label htmlFor="idcustomer" className="form-label">
                                    Customer
                                    <input
                                        type="number"
                                        id="idcustomer"
                                        name="idcustomer"
                                        className="form-input"
                                        required="required"
                                    />
                                </label>
                            </div>
                            <div className="form-group">
                                <label htmlFor="endDate" className="form-label">
                                    End Date:
                                    <input
                                        type="datetime-local"
                                        id="endDate"
                                        name="endDate"
                                        className="form-input-date"
                                        required="required"
                                    />
                                </label>
                                <label htmlFor="startDate" className="form-label">
                                    Start Date:
                                    <input
                                        type="datetime-local"
                                        id="startDate"
                                        name="startDate"
                                        className="form-input-date"
                                        required="required"
                                    />
                                </label>
                            </div>
                            <div className="form-group">
                                <label htmlFor="ip1" className="form-label">
                                    IP 1:
                                    <input
                                        type="text"
                                        id="ip1"
                                        name="ip1"
                                        className="form-input"
                                        required="required"
                                    />
                                </label>
                                <label htmlFor="ip2" className="form-label">
                                    IP 2:
                                    <input
                                        type="text"
                                        id="ip2"
                                        name="ip2"
                                        className="form-input"
                                        required="required"
                                    />
                                </label>
                            </div>
                            <div className="form-group">
                                <label htmlFor="ip3" className="form-label">
                                    IP 3:
                                    <input
                                        type="text"
                                        id="ip3"
                                        name="ip3"
                                        className="form-input"
                                        required="required"
                                    />
                                </label>
                                <label htmlFor="featureA" className="form-label">
                                    Feature A:
                                    <input
                                        type="number"
                                        id="featureA"
                                        name="featureA"
                                        className="form-input"
                                        required="required"
                                    />
                                </label>
                            </div>
                            <div className="form-group">
                                <label htmlFor="featureB" className="form-label">
                                    Feature B:
                                    <input
                                        type="number"
                                        id="featureB"
                                        name="featureB"
                                        className="form-input"
                                        required="required"
                                    />
                                </label>
                                <label htmlFor="featureC" className="form-label">
                                    Feature C:
                                    <input
                                        type="number"
                                        id="featureC"
                                        name="featureC"
                                        className="form-input"
                                        required="required"
                                    />
                                </label>
                            </div>
                            <div className="form-group">
                                <label htmlFor="version" className="form-label">
                                    Version:
                                    <input
                                        type="text"
                                        id="version"
                                        name="version"
                                        className="form-input"
                                        required="required"
                                    />
                                </label>
                                <label htmlFor="license" className="form-label">
                                    License:
                                    <input
                                        type="text"
                                        id="license"
                                        name="license"
                                        className="form-input-license"
                                        required="required"
                                    />
                                </label>
                            </div>
                            <button className="contracts-button" type="submit">
                                Save
                            </button>
                            <button
                                className="contracts-button"
                                type="button"
                                onClick={closeModalAdd}
                            >
                                Cancel
                            </button>
                        </form>
                    </div>
                </div>
            )}
        </div>
    );
};

export default Contracts;
