import React, {useEffect, useState} from "react";

import axios from "axios";
import {Link, useLocation, useNavigate, useParams} from "react-router-dom";
import "../Contracts/Contracts.css"
const initialState = {
    contract_name :"",
    startDatum  : "",
    endDatum :"",
    ip1 : "",
    ip2 : "",
    ip3 : "",
    numfeldFa1 : "",
    numfeldFa2 : "",
    numfeldFa3 : "",
    version : "",
    texfeldLizenz : "",
}
const ContractsUpdate=() =>{
        const [state, setState] = useState(initialState);
        const {contract_name, startDatum, endDatum, ip1, ip2, ip3, numfeldFa1, numfeldFa2, numfeldFa3, version, texfeldLizenz} = state;
        const navigate = useNavigate();
        const {id} = useParams();
        const location = useLocation();


        if(id){
            useEffect( ( ) => {
                axios
                    .get(`http://localhost:8080/contracts/${id}`)
                    .then((resp) => setState({... resp.data}));
                {console.log("HALLOID " + id)}
            }, [id])
        }


    const handleSubmit=(e) =>{
        e.preventDefault();
        if(!contract_name){
            alert("Bitte name eingeben!!!")
            //  toast.error(" Plase provide value into each input Field")
        } else{

                axios.put(`http://localhost:8080/contracts/updateContracts/${id}`, {
                    contract_name,
                    startDatum,
                    endDatum,
                    ip1,
                    ip2,
                    ip3,
                    numfeldFa1,
                    numfeldFa2,
                    numfeldFa3,
                    version,
                    texfeldLizenz
                })
                    .then(()=>{
                        setState({ contract_name: "", startDatum: "", endDatum: "", ip1: "", ip2: "", ip3: "", numfeldFa1: "", numfeldFa2: "", numfeldFa3: "", texfeldLizenz: "", version: ""});
                    })
                    .catch((err) => console.log(err));
                //   toast.success("Contact Added Successfully")
            }
            setTimeout(() => navigate("/Contracts/manage/"), 500);
        }
    const handleInputChange = (e) => {
        const {name, value} = e.target;
        setState({...state, [name]: value})
    }
    return (
    <div style={{marginTop: "100px"}}>
        <form style={{
            margin:"auto",
            padding: "15px",
            maxWidth: "400px",
            alignContent: "center"
        }}
              onSubmit={handleSubmit}
        >
            <label htmlFor="contract_name">contract_name</label>
            <input
                type="text"
                id="contract_name"
                name ="contract_name"
                placeholder='Please select your contract_name'
                value={contract_name || ''}
                onChange={handleInputChange}
            />
            <div class="dateStart">
            <label htmlFor="startDatum">startDatum</label>
            <input
                type="datetime-local"
                id="startDatum"
                name ="startDatum"
                placeholder='Your startDatum'
                value={startDatum || ""}
                onChange={handleInputChange}
            />
            </div>
            <div class="dateEnd">
            <label htmlFor="endDatum">endDatum</label>
            <input
                type="datetime-local"
                id="endDatum"
                name ="endDatum"
                placeholder='Your endDatum...'
                value={endDatum || ""}
                onChange={handleInputChange}
            />
            </div>
            <label htmlFor="ip1">ip1</label>
            <input
                type="number"
                id="ip1"
                name ="ip1"
                placeholder='Your ip1...'
                value={ip1 || ""}
                onChange={handleInputChange}
            />
            <label htmlFor="ip2">ip2</label>
            <input
                type="number"
                id="ip2"
                name ="ip2"
                placeholder='Your ip2...'
                value={ip2 || ""}
                onChange={handleInputChange}
            />
            <label htmlFor="ip3">ip3</label>
            <input
                type="number"
                id="ip3"
                name ="ip3"
                placeholder='Your ip3...'
                value={ip3 || ""}
                onChange={handleInputChange}
            />
            <label htmlFor="numfeldFa1">numfeldFa1</label>
            <input
                type="number"
                id="numfeldFa1"
                name ="numfeldFa1"
                placeholder='Your numfeldFa1...'
                value={numfeldFa1 || ""}
                onChange={handleInputChange}
            />
            <label htmlFor="numfeldFa2">numfeldFa2</label>
            <input
                type="number"
                id="numfeldFa2"
                name ="numfeldFa2"
                placeholder='Your numfeldFa2...'
                value={numfeldFa2 || ""}
                onChange={handleInputChange}
            />
            <label htmlFor="numfeldFa3">numfeldFa3</label>
            <input
                type="number"
                id="numfeldFa3"
                name ="numfeldFa3"
                placeholder='Your numfeldFa3...'
                value={numfeldFa3 || ""}
                onChange={handleInputChange}
            />
            <label htmlFor="version">version</label>
            <input
                type="number"
                id="version"
                name ="version"
                placeholder='Your version...'
                value={version || ""}
                onChange={handleInputChange}
            />
            <label htmlFor="texfeldLizenz">texfeldLizenz</label>
            <input
                type="number"
                id="texfeldLizenz"
                name ="texfeldLizenz"
                placeholder='Your texfeldLizenz...'
                value={texfeldLizenz || ""}
                onChange={handleInputChange}
            />
            <input type="submit" value={id ? "Update" : "Save"}/>
            <Link to="/Contracts/manage">
                <input type="button" value="Go Back"/>
            </Link>
        </form>
    </div>
    )
}
export default ContractsUpdate