import React, {useEffect, useState} from "react";
import {Link, useParams} from "react-router-dom";
import axios from "axios";

const ContractsView = () => {
    const [Contracts, setContracts] = useState({})
    const [Customer, setCustomer] = useState({});
    const {id} = useParams();
    useEffect( ( ) => {
        axios
            .get(`http://localhost:8080/contracts/${id}`)
            .then((resp) => setContracts({... resp.data}));
    }, [id])


    useEffect( ( ) => {
        axios
            .get("http://localhost:8080/customers")
            .then((resp) => setCustomer({... resp.data}));
    }, [id])

   // const newContracts = [];
   // const newContracts = [];
    // for (const key in Contracts) {
    //     let check = Contracts[key].idcustomer;
    //     if(check) {
    //         if(check.idcustomers == id){
    //             newContracts.push(Contracts[key]);
    //         }
    //     }
    // }

    //
    // const newUsers = [];
    // for (const key in User) {
    //     let check = User[key].idcustomer;
    //     if(check) {
    //         if(check.idcustomers == id) {
    //             newUsers.push(User[key]);
    //         }
    //
    //     }
    // }
return (
    <div style={{marginTop: "150px"}}>

    <div className="card">
        <div className="card-header">
            <p>Contract Detail</p>
        </div>
         <div className="container">
                <strong>Contract name: </strong>
                <span>{Contracts.contract_name}</span>
                <br />
                <br />
                <strong>Contract start date: </strong>
                <span>{Contracts.startDatum}</span>
                <br />
                <br />
                <strong>Contract end date: </strong>
                <span>{Contracts.endDatum}</span>
                <br />
                <br />
                <strong>ip1: </strong>
                <span>{Contracts.ip1}</span>
                <br />
                <br />
                <strong>ip2: </strong>
                <span>{Contracts.ip2}</span>
                <br />
                <br />
                <strong>ip3: </strong>
                <span>{Contracts.ip3}</span>
                <br />
                <br />
                <strong>Feature A: </strong>
                <span>{Contracts.numfeldFa1}</span>
                <br />
                <br />
                <strong>Feature B : </strong>
                <span>{Contracts.numfeldFa2}</span>
                <br />
                <br />
                <strong>Feature C : </strong>
                <span>{Contracts.numfeldFa3}</span>
                <br />
                <br />
                <strong>Version : </strong>
                <span>{Contracts.version}</span>
                <br />
                <br />
                <strong>Licence : </strong>
                <span>{Contracts.texfeldLizenz}</span>
                <br />
                <br />
                <strong>Customer : </strong>
                <span> {typeof Contracts.idcustomer === 'object'
                    ? JSON.stringify(Contracts.idcustomer.cusname)
                    : Contracts.cusname}</span>
                <br />
                {/*<br />*/}
                {/*{newUsers.map((item) => {*/}
                {/*    return(*/}
                {/*        <div>*/}
                {/*            <strong>Related User: </strong>*/}
                {/*            <span>{item.firstname}</span>*/}
                {/*            <br />*/}
                {/*            <br />*/}
                {/*        </div>*/}

                {/*    )*/}
                {/*})}*/}
            </div>
        <br />
        <br />
        <Link to="/Contracts/manage">
            <div className="btn btn-edit">Go Back</div>
        </Link>
    </div>
    </div>

)}

export default ContractsView
