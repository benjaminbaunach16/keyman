import React, {useState, useEffect} from "react";
import {} from "react-router-dom";
import "../Css/Users.css";
import axios from "axios";
import { Link } from "react-router-dom";
import * as url from "../../Url";

const theUrl =url.path;



const Users = ()=>{
    const [data, setData] = useState([])
    const [contracts, setContracts] = useState("")
    const [userContracts, setUserContracts] = useState([]);
    const [showModal, setShowModal] = useState(false);


    useEffect(() => {
        axios
            .get(`http://localhost:8080/contracts/userContracts`)
            .then((resp) => setUserContracts({ ...resp.data }));
    }, []);

    useEffect(() => {

        // Make a request to the server to get the user data
        axios
            .get("http://localhost:8080/contracts")
            .then(response => {
                setContracts(response.data);
            })
            .catch(error => {
                console.error(error);
            });

    }, []);
    const loadData = async()=>{
        const response = await axios.get(theUrl + "/users");
        setData(response.data);
    };
    useEffect(()=>{
        loadData();
    }, []);



    const deleteContact =(id)=>{
        let checker = true;


        if(Object.keys(userContracts).length <= 0){
            checker = false;
        }

        console.log("test")
        console.log(userContracts);

        for(let i = 0; i < Object.keys(userContracts).length; i++){
            if(id == userContracts[i].user.id){

               setShowModal(true);
               checker = true;
               break;
            } else{
                checker = false;
            }
        }
        console.log(checker);
    if(!checker) {
        if (window.confirm("Are you sure that you wanted to delete that User ?")) {
            axios.delete(theUrl + `/users/${id}`);
            //  toast.success("User deleted successfully");
            setTimeout(() => loadData(), 500);
        }
    }
    }


    function closeModal() {
        setShowModal(false);


    }



    return (
    <div style={{marginTop: "150px"}}>
            <Link to={"/Users/manage/addUser"}>
            <button className="btn btn-addUser" >AddUser</button>
            </Link>

        <table className="styled-table">
                <thead>
                <tr>
                    <th style={{textAlign:"center"}}>ID</th>
                    <th style={{textAlign:"center"}}>Admin</th>
                    <th style={{textAlign:"center"}}>E-Mail</th>
                    <th style={{textAlign:"center"}}>First Name</th>
                    <th style={{textAlign:"center"}}>Last Name</th>
                    <th style={{textAlign:"center"}}>phone 1</th>
                    <th style={{textAlign:"center"}}>phone 2</th>
                    <th style={{textAlign:"center"}}>username</th>
                    <th style={{textAlign:"center"}}>Customer</th>
                    <th style={{textAlign:"center"}}>Action</th>
                </tr>
                </thead>
                <tbody>
                {data.map((item, index)=>{
                    return (
                        <tr>
                            <td>{item.id}</td>
                            <td>{item.isAdmin}</td>
                            <td>{item.email}</td>
                            <td>{item.firstname}</td>
                            <td>{item.lastname}</td>
                            <td>{item.phone1}</td>
                            <td>{item.phone2}</td>
                            <td>{item.username}</td>
                            <td>
                                {typeof item.idcustomer === 'object'
                                    ? JSON.stringify(item.idcustomer.name)
                                    : item.name}
                            </td>
                            <td>

                                <Link to={`/Users/manage/updateUser/${item.id}`}>
                                    <button className="btn btn-edit">Edit</button>
                                </Link>
                                <button className="btn btn-delete" onClick={()=>deleteContact(item.id)}>Delete</button>
                                <Link to={`/Users/manage/view/${item.id}`}>
                                    <button className="btn btn-view">View</button>
                                </Link>
                            </td>


                        </tr>
                    )
                })}
                </tbody>
            {showModal && (
                <div className="customer-modal">
                    <div className="modal-center">
                        <h1>
                            Can't delete, because contracts are related! <br />
                            Delete related contracts first!
                        </h1>


                        <button className="button-close" onClick={closeModal}>
                            Close
                        </button>
                    </div>
                </div>
            )}
            </table>
        </div>

    );
}
export default  Users

