import React , {useState, useEffect} from 'react'
import {useNavigate, useParams, Link} from "react-router-dom"
import "../Css/AddEdit.css";
import axios from "axios";
import { useLocation } from 'react-router-dom';
const initialState = {
    idcustomer:  "",

}
export const AddEditCustomerid = () => {
    const [state, setState] = useState(initialState);
    const {idcustomer} = state;
    const navigate = useNavigate();
    const {id} = useParams();
    console.log(id)
    const location = useLocation()
    if(id !== undefined){
        useEffect(()=>{
            axios.get(`http://localhost:8080/users/updateUser/${id}`)
                .then((resp) => setState({...resp.data}))
        }, [id])
    }
    let iduser="";
    let check = location.pathname.match(/CustomerID/gi);

    if(!check){
         iduser= location.pathname.slice(22, 25)

    }
    else{
         iduser=location.pathname.slice(25, 28)
    }
    const handleSubmit=(e) => {
        e.preventDefault();


        if (!check) {
            axios.post(`http://localhost:8080/users/addUser/${iduser}/${idcustomer}`, {
                idcustomer,
            }).then(() => {
                setState({
                    idcustomer: ""
                })

            }).catch((err) => console.log(err));
            setTimeout(() =>
                navigate("/Users/manage"), 500);

        }
        else{
            axios.put(`http://localhost:8080/users/updateUser/${iduser}/CustomerID/${idcustomer}`, {

                idcustomer,
            }).then(() => {
                setState({
                    idcustomer: ""
                })

            }).catch((err) => console.log(err));
            setTimeout(() =>
                navigate("/Users/manage"), 500);

        }
    }
        const handleInputChange = (e) => {
            const {name, value} = e.target;
            setState({...state, [name]: value})
        }


        return (
            <div style={{marginTop: "100px"}}>
                <form style={{
                    margin: "auto",
                    padding: "15px",
                    maxWidth: "400px",
                    alignContent: "center"
                }}
                      onSubmit={handleSubmit}
                >

                    <label htmlFor="idcustomer">idcustomer</label>
                    <input
                        type="number"
                        id="idcustomer"
                        name="idcustomer"
                        placeholder='Your idcustomer...'
                        value={idcustomer || ""}
                        onChange={handleInputChange}
                    />

                    <input type="submit" value={id ? "Update" : "Save"}/>
                    <Link to="/Users/manage">
                        <input type="button" value="Go Back"/>
                    </Link>
                </form>
            </div>
        )

}

export default AddEditCustomerid