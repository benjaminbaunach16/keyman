import React, { useState, useEffect } from "react";
import { Link, useNavigate, useParams } from "react-router-dom";
import "../Css/AddEdit.css";
import axios from "axios";
import * as url from "../../Url";

const theUrl =url.path;

const initialState = {
    idUser: "",
    isAdmin: "",
    email: "",
    firstname:  "",
    lastname:  "",
    phone1:  "",
    phone2:  "",
    username:  "",
    idcustomer:  "",
    password: "",

}
export const AddEdit = () => {
    const [state, setState] = useState(initialState);
    const [contracts, setContracts] = useState("");
    const [customer, setCustomer] = useState("");
    const [userContracts, setUserContracts] = useState({});
    const [errorModal, setErrorModal] = useState(false);
    const [errorModal2, setErrorModal2] = useState(false);
    const [message, setMessage] = useState("")


    let {
        isAdmin,
        phone1,
        phone2,
        username,
        idcustomer,
    } = state;
    const navigate = useNavigate();
    const {id} = useParams();

   useEffect(() => {
        axios
            .get(`http://localhost:8080/contracts/userContracts`)
            .then((resp) => setUserContracts({ ...resp.data }));
    }, []);

    useEffect(() => {
        // Make a request to the server to get the user data
        axios
            .get("http://localhost:8080/contracts")
            .then((response) => {
                setContracts(response.data);
            })
            .catch((error) => {
                console.error(error);
            });
    }, []);

    useEffect(() => {
        // Make a request to the server to get the user data
        axios
            .get("http://localhost:8080/customers")
            .then((response) => {
                setCustomer(response.data);
            })
            .catch((error) => {
                console.error(error);
            });
    }, []);

    if(id!=undefined){
        useEffect(()=>{
            axios.get(theUrl + `/users/updateUser/${id}`)
                .then((resp) => setState({...resp.data}))
        }, [id])
    }


    const handleSubmit = (e) => {
        e.preventDefault();

        let customerExists = false;
        let contractExist = false;

        console.log( e.target.idservicecontracts.value)

        //check for Contract ID && check for Customer ID
        if(e.target.idservicecontracts.value || e.target.idcustomer.value) {

            for (let i = 0; i < contracts.length; i++) {
                if (contracts[i].idservicecontracts == e.target.idservicecontracts.value) {
                    contractExist = true;
                    break;
                }
            }

            for (let i = 0; i < customer.length; i++) {
                if (customer[i].idcustomers == e.target.idcustomer.value) {
                    customerExists = true;
                    break;
                }
            }

            if(!contractExist && !customerExists){
                setMessage("Contract with Contract ID: " + e.target.idservicecontracts.value + " doesn't exits!" + "\n Customer with Customer ID: " + e.target.idcustomer.value + " doesn't exits!")
                setErrorModal(true)
                return

            }

            else if (!contractExist &&  e.target.idservicecontracts.value) {
                 setMessage("Contract with Contract ID: " + e.target.idservicecontracts.value + " doesn't exits!")
                setErrorModal(true)
                return
            }

            else if (!customerExists) {
                 setMessage("Customer with Customer ID: " + e.target.idcustomer.value + " doesn't exits!")
                setErrorModal(true)
                return
            }
        }


        //check if there are more than 2 contracts used
        let timer = 0;
        if (e.target.idservicecontracts.value) {
            let check = userContracts[0];
            if (check) {
                let contractArray = [];
                for (let key in userContracts) {
                    contractArray.push({
                        key: userContracts[key].serviceContract.idservicecontracts,
                    });
                }
                for (let i = 0; i < contractArray.length; i++) {
                    if (e.target.idservicecontracts.value == contractArray[i].key) {
                        timer++;

                    }
                }
            }
        }

        if (timer >= 2) {
            e.target.idservicecontracts.value = "";
            setMessage("There are already users with this contract. Please change Contract ID!")
            setErrorModal(true);
        }
        else {
            let idservicecontracts = 0
            if(e.target.idservicecontracts.value) {
                idservicecontracts = e.target.idservicecontracts.value
            }
            idcustomer = e.target.idcustomer.value
            username = e.target.username.value;

            const newUser = {
                id: id,
                isAdmin: e.target.isAdmin.value,
                email: e.target.email.value,
                firstname: e.target.firstname.value,
                lastname: e.target.lastname.value,
                phone1: e.target.phone1.value,
                phone2: e.target.phone2.value,
                password: e.target.password.value
            };
        if(!id) {
            newUser.username = username;

            if(!newUser.phone1){
               newUser.phone1 = 0;
            }
            if(!newUser.phone2){
                newUser.phone2 = 0;
            }

            axios.post(theUrl + `/users/addUser1/${idcustomer}/${idservicecontracts}`, newUser)
                .then((response) => {
                    console.log(response.data);
                })
                .catch((error) => {
                    console.error(error);
                });
            setTimeout(() =>
                navigate("/Users/manage/"), 500);
        }else {

                if(!idcustomer){
                    idcustomer = 0;
                }
                if(!username) {
                  username = "not"
                }
                if(!newUser.phone1){
                    newUser.phone1 = phone1;
                }
                if(!newUser.phone2){
                    newUser.phone2 = phone2;
                }
                if(!newUser.isAdmin){
                    newUser.isAdmin = isAdmin;
                }

                axios.put(theUrl + `/users/updateUserByAdmin/${username}/${idcustomer}/${idservicecontracts}`, newUser)
                    .then((response) => {
                        console.log(response.data);
                    })
                    .catch((error) => {
                        console.error(error);
                    });
                setTimeout(() =>
                    navigate("/Users/manage/"), 500);
            }
         }
    };

    const handleInputChange = (e) => {
        const {name, value} = e.target;
        setState({...state, [name]: value})
    }

    return (
        <div className="add-container">
            <div className="customers-container">
                <table className="styled-table">
                    <thead>
                    <tr>
                        <th style={{ textAlign: "center" }}>ID</th>
                        <th style={{ textAlign: "center" }}>Customer Name</th>
                    </tr>
                    </thead>
                    {customer && (
                        <tbody>
                        {customer.map((customer, index) => (
                            <tr>
                                <td>{customer.idcustomers}</td>
                                <td>{customer.name}</td>
                            </tr>
                        ))}
                        </tbody>
                    )}
                </table>
            </div>

            <div className="addEddit">
                <form onSubmit={handleSubmit}>
                    <div className="input-container">
                        <div className="seperateTwo">
                            <label>Admin</label>
                            <div className="seperateOne">
                                <input
                                    className="input-styale"
                                    type="text"
                                    id="isAdmin"
                                    name="isAdmin"
                                    placeholder="Is Admin..."
                                />
                            </div>
                            <label>E-Mail</label>
                            <div className="seperateOne">
                                <input
                                    className="input-styale"
                                    type="email"
                                    id="email"
                                    name="email"
                                    placeholder="Your email..."
                                />
                            </div>
                        </div>
                        <div className="seperateTwo">
                            <label>firstname</label>
                            <div className="seperateOne">
                                <input
                                    className="input-styale"
                                    type="text"
                                    id="firstname"
                                    name="firstname"
                                    placeholder="Your firstname..."
                                />
                            </div>
                            <label>lastname</label>
                            <div className="seperateOne">
                                <input
                                    className="input-styale"
                                    type="text"
                                    id="lastname"
                                    name="lastname"
                                    placeholder="Your lastname..."
                                />
                            </div>
                        </div>
                        <div className="seperateTwo">
                            <label>phone1</label>
                            <div className="seperateOne">
                                <input
                                    className="input-styale"
                                    type="number"
                                    id="phone1"
                                    name="phone1"
                                    placeholder="Your phone1..."
                                />
                            </div>
                            <label>Phone2</label>
                            <div className="seperateOne">
                                <input
                                    className="input-styale"
                                    type="number"
                                    id="phone2"
                                    name="phone2"
                                    placeholder="Your phone2..."
                                />
                            </div>
                        </div>
                        <div className="seperateTwo">
                            <label>Username</label>
                            <div className="seperateOne">
                                <input
                                    className="input-styale"
                                    type="text"
                                    id="username"
                                    name="username"
                                    placeholder="Your username..."
                                />
                            </div>
                            <label>Customer ID</label>
                            <div className="seperateOne">
                                <input
                                    className="input-styale"
                                    type="number"
                                    id="idcustomer"
                                    name="idcustomer"
                                    placeholder="Your ID Customer..."
                                    required="required"
                                />
                            </div>
                        </div>
                        <div className="seperateTwo">
                            <label>Contracts ID</label>
                            <div className="seperateOne">
                                <input
                                    className="input-styale"
                                    type="number"
                                    id="idservicecontracts"
                                    name="idservicecontracts"
                                    placeholder="ID Service Contracts..."
                                />
                            </div>
                            <label>Password</label>
                            <div className="seperateOne">
                                <input
                                    className="input-styale"
                                    type="password"
                                    id="password"
                                    name="password"
                                    placeholder="User Password..."
                                />
                            </div>
                        </div>
                    </div>
                    <input type="submit" />
                    <Link to="/Users/manage">
                        <input type="button" value="Go Back" />
                    </Link>
                </form>
            </div>
            <div className="customers-container">
                <table className="styled-table">
                    <thead>
                    <tr>
                        <th style={{ textAlign: "center" }}>ID</th>
                        <th style={{ textAlign: "center" }}>Contract Name</th>
                        <th style={{ textAlign: "center" }}>Related to Customer</th>
                    </tr>
                    </thead>
                    {contracts && (
                        <tbody>
                        {contracts.map((contracts, index) => (
                            <tr>
                                <td>{contracts.idservicecontracts}</td>
                                <td>{contracts.contract_name}</td>
                                <td>{typeof contracts.idcustomer === 'object'
                                    ? JSON.stringify(contracts.idcustomer.name)
                                    : contracts.name}
                                </td>
                            </tr>
                        ))}
                        </tbody>
                    )}
                </table>
            </div>
            {errorModal && (
                 <div className="modal">
                    <div className="modal-content">
                         <h1 className="add-h1">
                             {message}
                         </h1>
                         <form onSubmit={handleSubmit} className="addEddit">
                             <div className="input-container">
                                 <div className="seperateTwo">
                                     <label>Admin</label>
                                    <div className="seperateOne">
                                         <input
                                            className="input-styale"
                                            type="text"
                                            id="isAdmin"
                                            name="isAdmin"
                                            placeholder="Is Admin..."
                                        />
                                    </div>
                                    <label>E-Mail</label>
                                    <div className="seperateOne">
                                        <input
                                            className="input-styale"
                                            type="email"
                                            id="email"
                                            name="email"
                                            placeholder="Your email..."
                                        />
                                    </div>
                                </div>
                                <div className="seperateTwo">
                                    <label>firstname</label>
                                    <div className="seperateOne">
                                        <input
                                            className="input-styale"
                                            type="text"
                                            id="firstname"
                                            name="firstname"
                                            placeholder="Your firstname..."
                                        />
                                    </div>
                                    <label>lastname</label>
                                    <div className="seperateOne">
                                        <input
                                            className="input-styale"
                                            type="text"
                                            id="lastname"
                                            name="lastname"
                                            placeholder="Your lastname..."
                                        />
                                    </div>
                                </div>
                                <div className="seperateTwo">
                                    <label>phone1</label>
                                    <div className="seperateOne">
                                        <input
                                            className="input-styale"
                                            type="number"
                                            id="phone1"
                                            name="phone1"
                                            placeholder="Your phone1..."
                                        />
                                    </div>
                                    <label>Phone2</label>
                                    <div className="seperateOne">
                                        <input
                                            className="input-styale"
                                            type="number"
                                            id="phone2"
                                            name="phone2"
                                            placeholder="Your phone2..."
                                        />
                                    </div>
                                </div>
                                <div className="seperateTwo">
                                    <label>Username</label>
                                    <div className="seperateOne">
                                        <input
                                            className="input-styale"
                                            type="text"
                                            id="username"
                                            name="username"
                                            placeholder="Your username..."
                                        />
                                    </div>
                                    <label>Customer ID</label>
                                    <div className="seperateOne">
                                        <input
                                            className="input-styale"
                                            type="number"
                                            id="idcustomer"
                                            name="idcustomer"
                                            placeholder="Your ID Customer..."
                                        />
                                    </div>
                                </div>
                                <div className="seperateTwo">
                                    <label>Contracts ID</label>
                                    <div className="seperateOne">
                                        <input
                                            className="input-styale"
                                            type="number"
                                            id="idservicecontracts"
                                            name="idservicecontracts"
                                            placeholder="ID Service Contracts..."
                                        />
                                    </div>
                                    <label>Password</label>
                                    <div className="seperateOne">
                                        <input
                                             className="input-styale"
                                             type="password"
                                             id="password"
                                            name="password"
                                             placeholder="Your Password..."
                                         />
                                     </div>
                                 </div>
                             </div>
                             <input type="submit" />
                             <Link to="/Users/manage">
                                <input type="button" value="Go Back" />
                             </Link>
                        </form>
                     </div>
                </div>
            )}
        </div>
    )

}

export default AddEdit;

