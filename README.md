# Keyman



Keyman" war ein Projekt, das im Rahmen eines Hochschulmoduls entwickelt wurde. Das Hauptziel bestand darin, eine robuste Softwarearchitektur aufzubauen, die eine effiziente Verwaltung von Schlüsseldaten ermöglichte. Das Projekt umfasste sowohl die Entwicklung des Frontends mit React als auch des Backends mit ORM (Object-Relational Mapping) und REST JaxRS. 
Im Frontend wurde React verwendet, eine leistungsstarke JavaScript-Bibliothek zur Erstellung von benutzerfreundlichen Webanwendungen. Durch die Verwendung von React konnten wir eine reaktive Benutzeroberfläche entwickeln, die eine nahtlose Interaktion mit den Schlüsseldaten ermöglichte. 

Das Frontend wurde mit modernen UI-Komponenten und einer intuitiven Benutzeroberfläche gestaltet, um eine angenehme Benutzererfahrung zu gewährleisten. Das Backend wurde mit ORM (Object-Relational Mapping) entwickelt, was eine effiziente Datenbankinteraktion ermöglichte. 

ORM erleichtert die Kommunikation zwischen der Anwendung und der Datenbank, indem es den Entwicklern ermöglicht, auf objektorientierte Weise auf die Daten zuzugreifen, anstatt direkt mit SQL-Abfragen arbeiten zu müssen. Dadurch konnten wir eine robuste und wartbare Datenzugriffsschicht erstellen.
Zusätzlich wurde für die Implementierung der API das REST JaxRS-Framework verwendet. REST steht für Representational State Transfer und ist ein Architekturstil, der es ermöglicht, Webdienste über das HTTP-Protokoll bereitzustellen. JaxRS ist eine Java-Spezifikation für die Entwicklung von RESTful Web Services. Die Verwendung von REST JaxRS erleichterte die Erstellung einer gut strukturierten und skalierbaren API, die es ermöglichte, Schlüsseldaten über HTTP-Endpunkte abzurufen, zu erstellen, zu aktualisieren und zu löschen.

Das Projekt "Keyman" stellte eine Herausforderung dar, bei der wir unsere Fähigkeiten im Bereich der Softwarearchitektur, des Frontend- und Backend-Entwicklungsprozesses sowie der Datenbankinteraktion weiterentwickeln konnten. 
Durch die Kombination von React, ORM und REST JaxRS konnten wir eine vollständige Lösung für die Verwaltung von Schlüsseldaten entwickeln, die den Anforderungen des Hochschulmoduls gerecht wurde und gleichzeitig praktische Erfahrungen in der Umsetzung einer umfangreichen Softwarearchitektur bo