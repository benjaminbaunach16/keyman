-- MySQL dump 10.13  Distrib 8.0.31, for Win64 (x86_64)
--
-- Host: localhost    Database: orm
-- ------------------------------------------------------
-- Server version	8.0.31

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `servicecontracts`
--

DROP TABLE IF EXISTS `servicecontracts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `servicecontracts` (
  `idservicecontracts` int NOT NULL,
  `iduser` int DEFAULT NULL,
  `idcustomer` int DEFAULT NULL,
  `start_datum` datetime DEFAULT NULL,
  `end_datum` datetime DEFAULT NULL,
  `ip_1` varchar(45) DEFAULT NULL,
  `ip_2` varchar(45) DEFAULT NULL,
  `ip_3` varchar(45) DEFAULT NULL,
  `version` varchar(45) DEFAULT NULL,
  `numfeld_fa1` int DEFAULT NULL,
  `numfeld_fa2` int DEFAULT NULL,
  `numfeld_fa3` int DEFAULT NULL,
  `texfeld_lizenz` varchar(254) DEFAULT NULL,
  PRIMARY KEY (`idservicecontracts`),
  KEY `contract_iduser_idx` (`iduser`),
  KEY `contract_idcustomer_idx` (`idcustomer`),
  CONSTRAINT `contract_idcustomer` FOREIGN KEY (`idcustomer`) REFERENCES `customers` (`idcustomers`),
  CONSTRAINT `contract_iduser` FOREIGN KEY (`iduser`) REFERENCES `user` (`iduser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `servicecontracts`
--

LOCK TABLES `servicecontracts` WRITE;
/*!40000 ALTER TABLE `servicecontracts` DISABLE KEYS */;
/*!40000 ALTER TABLE `servicecontracts` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-12-01 15:53:52
